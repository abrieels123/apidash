<?php
/**
Template Name: quoteOrder
 */
$cnnectionPath = getcwd(); // Current working directory where we are
$connection = "$cnnectionPath/wp-content/themes/thursday/quoteConverter/connection.php"; // connection credential and pdo instance 
require_once($connection);  // Include connection file in the code
//var_dump($_POST); die();
$stmts = $pdo->prepare("SELECT * FROM `quotes` where quoteNumber = '".$_GET['quote']."' "); // get the quotes information
$stmts->execute();
$row = $stmts->fetch();
$FirstName = $row['name']; // First name for specific quotes
$surname = $row['surname'];
$phone = $row['phone'];
$email = $row['email'];
$dbEmail = $row['email'];
$stmts = $pdo->prepare("UPDATE `quotes` set status = 'Approved' where quoteNumber = '".$_GET['quote']."' "); // change status of quotes to Approved
$stmts->execute();
if(isset($_POST["billing_name"]))
{ 
    //print_r("<pre>");var_dump($_POST); die();
    $exists = email_exists( $_POST['billing_email'] );
    if ( $exists ) {
        $user = get_user_by( 'email', $_POST['billing_email'] );
        $userid = $user->ID;
        $order = wc_create_order(array('customer_id'=>$userid));
    } else {
        $order = wc_create_order();
    }
        
        $fname     = $_POST["billing_name"]; //$user->first_name;
        $lname     = $_POST["billing_surname"]; //$user->last_name;
        $email     = $_POST['billing_email']; //$user->user_email;
        $address_1 = $_POST['billing_address_1']; //get_user_meta( $userId, 'billing_address_1', true );
        $address_2 = $_POST['billing_address_2']; //get_user_meta( $userId, 'billing_address_2', true );
        $city      = $_POST['billing_city']; //get_user_meta( $userId, 'billing_city', true );
        $postcode  = $_POST['shipping_zip']; //get_user_meta( $userId, 'billing_postcode', true );
        $country   = $_POST['country']; //get_user_meta( $userId, 'billing_country', true );
        $state     = $_POST['province']; //get_user_meta( $userId, 'billing_state', true );
        $phone     = $_POST['billing_phone']; //get_user_meta( $userId, 'billing_state', true )
        
        $fname1     = $_POST['shipping_first_name']; //$user->first_name;
        $lname1     = $_POST['shipping_surname']; //$user->last_name;
        $email1     = $_POST['shipping_email']; //$user->user_email;
        $company1 = $_POST['shipping_company'];
        $address_11 = $_POST['shipping_add1']; //get_user_meta( $userId, 'billing_address_1', true );
        $address_22 = $_POST['shipping_add2']; //get_user_meta( $userId, 'billing_address_2', true );
        $city1      = $_POST['shipping_city']; //get_user_meta( $userId, 'billing_city', true );
        $postcode1  = $_POST['shipping_zip']; //get_user_meta( $userId, 'billing_postcode', true );
        $country1   = $_POST['shipping_country']; //get_user_meta( $userId, 'billing_country', true );
        $state1     = $_POST['shipping_province']; //get_user_meta( $userId, 'billing_state', true );
        $phone1     = $_POST['shipping_phone']; //get_user_meta( $userId, 'billing_state', true )
        $address1         = array(
            'first_name' => $fname,
            'last_name'  => $lname,
            'phone'      => $phone,
            'email'      => $email,
            'address_1'  => $address_1,
            'address_2'  => $address_2,
            'city'       => $city,
            'state'      => $state,
            'postcode'   => $postcode,
            'country'    => $country,
        );
        
        $address2         = array(
            'first_name' => $fname1,
            'last_name'  => $lname1,
            'company'  => $company1,
            'email'      => $email1,
            'phone'      => $phone1,
            'address_1'  => $address_11,
            'address_2'  => $address_22,
            'city'       => $city1,
            'state'      => $state1,
            'postcode'   => $postcode1,
            'country'    => $country1,
        );
    
        $order->set_address( $address1, 'billing' );
        $order->set_address( $address2, 'shipping' );
        
         
        $stmts = $pdo->prepare("SELECT * FROM `qouteDetails` where `quoteNumber` = '".$_GET['quote']."' "); // get the quotes information
        $stmts->execute();
        while ($row = $stmts->fetch()) {
            $factory= new WC_Product_Factory();     
            $product = $factory->get_product($row['product_id']);
            $order->add_product( $product, $row['quantity'] );
        }
        $payment_gateways = WC()->payment_gateways->payment_gateways();
        $order->set_payment_method( $payment_gateways['cod'] ); 
        $order->calculate_totals(); // Update order taxes and totals
        $note = __(trim($_POST['shipping_notes']));
        // Add the note
        $order->add_order_note( $note );
        $order->transaction_id = $_POST['shipping_vat'];
        $order->update_status( 'completed', 'In Store ', true );
        $order->save();
}

get_header('quotation');
?>
<br/><br/><br/><br/><br/><br/><br/><br/><br/>

<?php
if(!isset($_POST["billing_name"]))
{
    ?>
<form action="" method="post">
<div style="padding: 20px;width: 100%; background-color: white;">
    <div class="row">
        <?php
        $email = $dbEmail;
$exists = email_exists( $email );
if ( $exists ) {
    $user = get_user_by( 'email', $dbEmail);
    $userid = $user->ID;
     $address_1 = get_user_meta( $userId, 'billing_address_1', true );
        $address_2 = get_user_meta( $userId, 'billing_address_2', true );
        $city      = get_user_meta( $userId, 'billing_city', true );
        $postcode  = get_user_meta( $userId, 'billing_postcode', true );
        $country   = get_user_meta( $userId, 'billing_country', true );
        $state     = get_user_meta( $userId, 'billing_state', true );
} else {
    $address_1 = "";
        $address_2 = "";
        $city      = "";
        $postcode  = "";
        $country   = "";
        $state     = "";
}
        
        
        ?>
        <div class="col-md-6" style="background-color: white;">
            <!-- top row Title -->
            <div class="row">
                <div class="col-md-6">
                   <h3>Billing details</h3>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-6">
                   <b>First name *</b>
                </div>
                <div class="col-md-6">
                   <b>Last name *</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                   <input type="text" class="form-control" name="billing_name" value="<?php echo $FirstName; ?>" >
                </div>
                <div class="col-md-6">
                   <input type="text" class="form-control" name="billing_surname"  value="<?php echo $surname; ?>" >
                </div> 
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Country / Region *</b> 
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <select class="form-control" name="country">
            <option value="AX" >&#197;land Islands</option><option value="AF" >Afghanistan</option><option value="AL" >Albania</option><option value="DZ" >Algeria</option><option value="AS" >American Samoa</option><option value="AD" >Andorra</option><option value="AO" >Angola</option><option value="AI" >Anguilla</option><option value="AQ" >Antarctica</option><option value="AG" >Antigua and Barbuda</option><option value="AR" >Argentina</option><option value="AM" >Armenia</option><option value="AW" >Aruba</option><option value="AU" >Australia</option><option value="AT" >Austria</option><option value="AZ" >Azerbaijan</option><option value="BS" >Bahamas</option><option value="BH" >Bahrain</option><option value="BD" >Bangladesh</option><option value="BB" >Barbados</option><option value="BY" >Belarus</option><option value="PW" >Belau</option><option value="BE" >Belgium</option><option value="BZ" >Belize</option><option value="BJ" >Benin</option><option value="BM" >Bermuda</option><option value="BT" >Bhutan</option><option value="BO" >Bolivia</option><option value="BQ" >Bonaire, Saint Eustatius and Saba</option><option value="BA" >Bosnia and Herzegovina</option><option value="BW" >Botswana</option><option value="BV" >Bouvet Island</option><option value="BR" >Brazil</option><option value="IO" >British Indian Ocean Territory</option><option value="BN" >Brunei</option><option value="BG" >Bulgaria</option><option value="BF" >Burkina Faso</option><option value="BI" >Burundi</option><option value="KH" >Cambodia</option><option value="CM" >Cameroon</option><option value="CA" >Canada</option><option value="CV" >Cape Verde</option><option value="KY" >Cayman Islands</option><option value="CF" >Central African Republic</option><option value="TD" >Chad</option><option value="CL" >Chile</option><option value="CN" >China</option><option value="CX" >Christmas Island</option><option value="CC" >Cocos (Keeling) Islands</option><option value="CO" >Colombia</option><option value="KM" >Comoros</option><option value="CG" >Congo (Brazzaville)</option><option value="CD" >Congo (Kinshasa)</option><option value="CK" >Cook Islands</option><option value="CR" >Costa Rica</option><option value="HR" >Croatia</option><option value="CU" >Cuba</option><option value="CW" >Cura&ccedil;ao</option><option value="CY" >Cyprus</option><option value="CZ" >Czech Republic</option><option value="DK" >Denmark</option><option value="DJ" >Djibouti</option><option value="DM" >Dominica</option><option value="DO" >Dominican Republic</option><option value="EC" >Ecuador</option><option value="EG" >Egypt</option><option value="SV" >El Salvador</option><option value="GQ" >Equatorial Guinea</option><option value="ER" >Eritrea</option><option value="EE" >Estonia</option><option value="ET" >Ethiopia</option><option value="FK" >Falkland Islands</option><option value="FO" >Faroe Islands</option><option value="FJ" >Fiji</option><option value="FI" >Finland</option><option value="FR" >France</option><option value="GF" >French Guiana</option><option value="PF" >French Polynesia</option><option value="TF" >French Southern Territories</option><option value="GA" >Gabon</option><option value="GM" >Gambia</option><option value="GE" >Georgia</option><option value="DE" >Germany</option><option value="GH" >Ghana</option><option value="GI" >Gibraltar</option><option value="GR" >Greece</option><option value="GL" >Greenland</option><option value="GD" >Grenada</option><option value="GP" >Guadeloupe</option><option value="GU" >Guam</option><option value="GT" >Guatemala</option><option value="GG" >Guernsey</option><option value="GN" >Guinea</option><option value="GW" >Guinea-Bissau</option><option value="GY" >Guyana</option><option value="HT" >Haiti</option><option value="HM" >Heard Island and McDonald Islands</option><option value="HN" >Honduras</option><option value="HK" >Hong Kong</option><option value="HU" >Hungary</option><option value="IS" >Iceland</option><option value="IN" >India</option><option value="ID" >Indonesia</option><option value="IR" >Iran</option><option value="IQ" >Iraq</option><option value="IE" >Ireland</option><option value="IM" >Isle of Man</option><option value="IL" >Israel</option><option value="IT" >Italy</option><option value="CI" >Ivory Coast</option><option value="JM" >Jamaica</option><option value="JP" >Japan</option><option value="JE" >Jersey</option><option value="JO" >Jordan</option><option value="KZ" >Kazakhstan</option><option value="KE" >Kenya</option><option value="KI" >Kiribati</option><option value="KW" >Kuwait</option><option value="KG" >Kyrgyzstan</option><option value="LA" >Laos</option><option value="LV" >Latvia</option><option value="LB" >Lebanon</option><option value="LS" >Lesotho</option><option value="LR" >Liberia</option><option value="LY" >Libya</option><option value="LI" >Liechtenstein</option><option value="LT" >Lithuania</option><option value="LU" >Luxembourg</option><option value="MO" >Macao</option><option value="MG" >Madagascar</option><option value="MW" >Malawi</option><option value="MY" >Malaysia</option><option value="MV" >Maldives</option><option value="ML" >Mali</option><option value="MT" >Malta</option><option value="MH" >Marshall Islands</option><option value="MQ" >Martinique</option><option value="MR" >Mauritania</option><option value="MU" >Mauritius</option><option value="YT" >Mayotte</option><option value="MX" >Mexico</option><option value="FM" >Micronesia</option><option value="MD" >Moldova</option><option value="MC" >Monaco</option><option value="MN" >Mongolia</option><option value="ME" >Montenegro</option><option value="MS" >Montserrat</option><option value="MA" >Morocco</option><option value="MZ" >Mozambique</option><option value="MM" >Myanmar</option><option value="NA" >Namibia</option><option value="NR" >Nauru</option><option value="NP" >Nepal</option><option value="NL" >Netherlands</option><option value="NC" >New Caledonia</option><option value="NZ" >New Zealand</option><option value="NI" >Nicaragua</option><option value="NE" >Niger</option><option value="NG" >Nigeria</option><option value="NU" >Niue</option><option value="NF" >Norfolk Island</option><option value="KP" >North Korea</option><option value="MK" >North Macedonia</option><option value="MP" >Northern Mariana Islands</option><option value="NO" >Norway</option><option value="OM" >Oman</option><option value="PK" >Pakistan</option><option value="PS" >Palestinian Territory</option><option value="PA" >Panama</option><option value="PG" >Papua New Guinea</option><option value="PY" >Paraguay</option><option value="PE" >Peru</option><option value="PH" >Philippines</option><option value="PN" >Pitcairn</option><option value="PL" >Poland</option><option value="PT" >Portugal</option><option value="PR" >Puerto Rico</option><option value="QA" >Qatar</option><option value="RE" >Reunion</option><option value="RO" >Romania</option><option value="RU" >Russia</option><option value="RW" >Rwanda</option><option value="ST" >S&atilde;o Tom&eacute; and Pr&iacute;ncipe</option><option value="BL" >Saint Barth&eacute;lemy</option><option value="SH" >Saint Helena</option><option value="KN" >Saint Kitts and Nevis</option><option value="LC" >Saint Lucia</option><option value="SX" >Saint Martin (Dutch part)</option><option value="MF" >Saint Martin (French part)</option><option value="PM" >Saint Pierre and Miquelon</option><option value="VC" >Saint Vincent and the Grenadines</option><option value="WS" >Samoa</option><option value="SM" >San Marino</option><option value="SA" >Saudi Arabia</option><option value="SN" >Senegal</option><option value="RS" >Serbia</option><option value="SC" >Seychelles</option><option value="SL" >Sierra Leone</option><option value="SG" >Singapore</option><option value="SK" >Slovakia</option><option value="SI" >Slovenia</option><option value="SB" >Solomon Islands</option><option value="SO" >Somalia</option><option value="ZA"  selected='selected'>South Africa</option><option value="GS" >South Georgia/Sandwich Islands</option><option value="KR" >South Korea</option><option value="SS" >South Sudan</option><option value="ES" >Spain</option><option value="LK" >Sri Lanka</option><option value="SD" >Sudan</option><option value="SR" >Suriname</option><option value="SJ" >Svalbard and Jan Mayen</option><option value="SZ" >Swaziland</option><option value="SE" >Sweden</option><option value="CH" >Switzerland</option><option value="SY" >Syria</option><option value="TW" >Taiwan</option><option value="TJ" >Tajikistan</option><option value="TZ" >Tanzania</option><option value="TH" >Thailand</option><option value="TL" >Timor-Leste</option><option value="TG" >Togo</option><option value="TK" >Tokelau</option><option value="TO" >Tonga</option><option value="TT" >Trinidad and Tobago</option><option value="TN" >Tunisia</option><option value="TR" >Turkey</option><option value="TM" >Turkmenistan</option><option value="TC" >Turks and Caicos Islands</option><option value="TV" >Tuvalu</option><option value="UG" >Uganda</option><option value="UA" >Ukraine</option><option value="AE" >United Arab Emirates</option><option value="GB" >United Kingdom (UK)</option><option value="US" >United States (US)</option><option value="UM" >United States (US) Minor Outlying Islands</option><option value="UY" >Uruguay</option><option value="UZ" >Uzbekistan</option><option value="VU" >Vanuatu</option><option value="VA" >Vatican</option><option value="VE" >Venezuela</option><option value="VN" >Vietnam</option><option value="VG" >Virgin Islands (British)</option><option value="VI" >Virgin Islands (US)</option><option value="WF" >Wallis and Futuna</option><option value="EH" >Western Sahara</option><option value="YE" >Yemen</option><option value="ZM" >Zambia</option><option value="ZW" >Zimbabwe</option>
>            </select> 
                </div></div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Street address *</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   <input type="text" class="form-control" placeholder="House Number And Street Name" name="billing_address_1" value="<?php echo $meta["billing_address_1"][0]; ?>">
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <input type="text" class="form-control" placeholder="Apartment, Suite, Unit, etc. (Optional)" name="billing_address_2"  value="<?php echo $address_2; ?>">
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Town / City *</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   <input type="text" class="form-control" placeholder="" name="billing_city"  value="<?php echo $city; ?>">
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Province *</b>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <select class="form-control" name="province">
            <option value="EC" >Eastern Cape</option><option value="FS" >Free State</option><option value="GP"  selected='selected'>Gauteng</option><option value="KZN" >KwaZulu-Natal</option><option value="LP" >Limpopo</option><option value="MP" >Mpumalanga</option><option value="NC" >Northern Cape</option><option value="NW" >North West</option><option value="WC" >Western Cape</option>
            </select>
            </div></div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Postcode / ZIP *</b>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-12">
                   <input type="text" class="form-control" placeholder="" name="billing_zip">
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Phone *</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   <input type="text" class="form-control" placeholder="" name="billing_phone"  value="<?php echo $phone; ?>" >
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Email address *</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   <input type="text" class="form-control" placeholder="" name="billing_email"  value="<?php echo $user->user_email; ?>" > 
                </div>
            </div>
            <br/>
            
            
            
            
            
            
            
            
        </div>
        
        <div class="col-md-6" style="background-color: white;"> 
          
            <br/>
            <div class="row">
                <div class="col-md-6">
                   <b>First name *</b>
                </div>
                <div class="col-md-6">
                   <b>Last name *</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                   <input type="text" class="form-control" name="shipping_name" value="<?php echo $FirstName; ?>">
                </div>
                <div class="col-md-6">
                   <input type="text" class="form-control" ame="shipping_surname" value="<?php echo $surname; ?>">
                </div> 
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Company Name (Optional)</b> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   <input type="text" class="form-control" name="shipping_company">
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Country / Region *</b> 
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <select class="form-control" name="shipping_country">
            <option value="AX" >&#197;land Islands</option><option value="AF" >Afghanistan</option><option value="AL" >Albania</option><option value="DZ" >Algeria</option><option value="AS" >American Samoa</option><option value="AD" >Andorra</option><option value="AO" >Angola</option><option value="AI" >Anguilla</option><option value="AQ" >Antarctica</option><option value="AG" >Antigua and Barbuda</option><option value="AR" >Argentina</option><option value="AM" >Armenia</option><option value="AW" >Aruba</option><option value="AU" >Australia</option><option value="AT" >Austria</option><option value="AZ" >Azerbaijan</option><option value="BS" >Bahamas</option><option value="BH" >Bahrain</option><option value="BD" >Bangladesh</option><option value="BB" >Barbados</option><option value="BY" >Belarus</option><option value="PW" >Belau</option><option value="BE" >Belgium</option><option value="BZ" >Belize</option><option value="BJ" >Benin</option><option value="BM" >Bermuda</option><option value="BT" >Bhutan</option><option value="BO" >Bolivia</option><option value="BQ" >Bonaire, Saint Eustatius and Saba</option><option value="BA" >Bosnia and Herzegovina</option><option value="BW" >Botswana</option><option value="BV" >Bouvet Island</option><option value="BR" >Brazil</option><option value="IO" >British Indian Ocean Territory</option><option value="BN" >Brunei</option><option value="BG" >Bulgaria</option><option value="BF" >Burkina Faso</option><option value="BI" >Burundi</option><option value="KH" >Cambodia</option><option value="CM" >Cameroon</option><option value="CA" >Canada</option><option value="CV" >Cape Verde</option><option value="KY" >Cayman Islands</option><option value="CF" >Central African Republic</option><option value="TD" >Chad</option><option value="CL" >Chile</option><option value="CN" >China</option><option value="CX" >Christmas Island</option><option value="CC" >Cocos (Keeling) Islands</option><option value="CO" >Colombia</option><option value="KM" >Comoros</option><option value="CG" >Congo (Brazzaville)</option><option value="CD" >Congo (Kinshasa)</option><option value="CK" >Cook Islands</option><option value="CR" >Costa Rica</option><option value="HR" >Croatia</option><option value="CU" >Cuba</option><option value="CW" >Cura&ccedil;ao</option><option value="CY" >Cyprus</option><option value="CZ" >Czech Republic</option><option value="DK" >Denmark</option><option value="DJ" >Djibouti</option><option value="DM" >Dominica</option><option value="DO" >Dominican Republic</option><option value="EC" >Ecuador</option><option value="EG" >Egypt</option><option value="SV" >El Salvador</option><option value="GQ" >Equatorial Guinea</option><option value="ER" >Eritrea</option><option value="EE" >Estonia</option><option value="ET" >Ethiopia</option><option value="FK" >Falkland Islands</option><option value="FO" >Faroe Islands</option><option value="FJ" >Fiji</option><option value="FI" >Finland</option><option value="FR" >France</option><option value="GF" >French Guiana</option><option value="PF" >French Polynesia</option><option value="TF" >French Southern Territories</option><option value="GA" >Gabon</option><option value="GM" >Gambia</option><option value="GE" >Georgia</option><option value="DE" >Germany</option><option value="GH" >Ghana</option><option value="GI" >Gibraltar</option><option value="GR" >Greece</option><option value="GL" >Greenland</option><option value="GD" >Grenada</option><option value="GP" >Guadeloupe</option><option value="GU" >Guam</option><option value="GT" >Guatemala</option><option value="GG" >Guernsey</option><option value="GN" >Guinea</option><option value="GW" >Guinea-Bissau</option><option value="GY" >Guyana</option><option value="HT" >Haiti</option><option value="HM" >Heard Island and McDonald Islands</option><option value="HN" >Honduras</option><option value="HK" >Hong Kong</option><option value="HU" >Hungary</option><option value="IS" >Iceland</option><option value="IN" >India</option><option value="ID" >Indonesia</option><option value="IR" >Iran</option><option value="IQ" >Iraq</option><option value="IE" >Ireland</option><option value="IM" >Isle of Man</option><option value="IL" >Israel</option><option value="IT" >Italy</option><option value="CI" >Ivory Coast</option><option value="JM" >Jamaica</option><option value="JP" >Japan</option><option value="JE" >Jersey</option><option value="JO" >Jordan</option><option value="KZ" >Kazakhstan</option><option value="KE" >Kenya</option><option value="KI" >Kiribati</option><option value="KW" >Kuwait</option><option value="KG" >Kyrgyzstan</option><option value="LA" >Laos</option><option value="LV" >Latvia</option><option value="LB" >Lebanon</option><option value="LS" >Lesotho</option><option value="LR" >Liberia</option><option value="LY" >Libya</option><option value="LI" >Liechtenstein</option><option value="LT" >Lithuania</option><option value="LU" >Luxembourg</option><option value="MO" >Macao</option><option value="MG" >Madagascar</option><option value="MW" >Malawi</option><option value="MY" >Malaysia</option><option value="MV" >Maldives</option><option value="ML" >Mali</option><option value="MT" >Malta</option><option value="MH" >Marshall Islands</option><option value="MQ" >Martinique</option><option value="MR" >Mauritania</option><option value="MU" >Mauritius</option><option value="YT" >Mayotte</option><option value="MX" >Mexico</option><option value="FM" >Micronesia</option><option value="MD" >Moldova</option><option value="MC" >Monaco</option><option value="MN" >Mongolia</option><option value="ME" >Montenegro</option><option value="MS" >Montserrat</option><option value="MA" >Morocco</option><option value="MZ" >Mozambique</option><option value="MM" >Myanmar</option><option value="NA" >Namibia</option><option value="NR" >Nauru</option><option value="NP" >Nepal</option><option value="NL" >Netherlands</option><option value="NC" >New Caledonia</option><option value="NZ" >New Zealand</option><option value="NI" >Nicaragua</option><option value="NE" >Niger</option><option value="NG" >Nigeria</option><option value="NU" >Niue</option><option value="NF" >Norfolk Island</option><option value="KP" >North Korea</option><option value="MK" >North Macedonia</option><option value="MP" >Northern Mariana Islands</option><option value="NO" >Norway</option><option value="OM" >Oman</option><option value="PK" >Pakistan</option><option value="PS" >Palestinian Territory</option><option value="PA" >Panama</option><option value="PG" >Papua New Guinea</option><option value="PY" >Paraguay</option><option value="PE" >Peru</option><option value="PH" >Philippines</option><option value="PN" >Pitcairn</option><option value="PL" >Poland</option><option value="PT" >Portugal</option><option value="PR" >Puerto Rico</option><option value="QA" >Qatar</option><option value="RE" >Reunion</option><option value="RO" >Romania</option><option value="RU" >Russia</option><option value="RW" >Rwanda</option><option value="ST" >S&atilde;o Tom&eacute; and Pr&iacute;ncipe</option><option value="BL" >Saint Barth&eacute;lemy</option><option value="SH" >Saint Helena</option><option value="KN" >Saint Kitts and Nevis</option><option value="LC" >Saint Lucia</option><option value="SX" >Saint Martin (Dutch part)</option><option value="MF" >Saint Martin (French part)</option><option value="PM" >Saint Pierre and Miquelon</option><option value="VC" >Saint Vincent and the Grenadines</option><option value="WS" >Samoa</option><option value="SM" >San Marino</option><option value="SA" >Saudi Arabia</option><option value="SN" >Senegal</option><option value="RS" >Serbia</option><option value="SC" >Seychelles</option><option value="SL" >Sierra Leone</option><option value="SG" >Singapore</option><option value="SK" >Slovakia</option><option value="SI" >Slovenia</option><option value="SB" >Solomon Islands</option><option value="SO" >Somalia</option><option value="ZA"  selected='selected'>South Africa</option><option value="GS" >South Georgia/Sandwich Islands</option><option value="KR" >South Korea</option><option value="SS" >South Sudan</option><option value="ES" >Spain</option><option value="LK" >Sri Lanka</option><option value="SD" >Sudan</option><option value="SR" >Suriname</option><option value="SJ" >Svalbard and Jan Mayen</option><option value="SZ" >Swaziland</option><option value="SE" >Sweden</option><option value="CH" >Switzerland</option><option value="SY" >Syria</option><option value="TW" >Taiwan</option><option value="TJ" >Tajikistan</option><option value="TZ" >Tanzania</option><option value="TH" >Thailand</option><option value="TL" >Timor-Leste</option><option value="TG" >Togo</option><option value="TK" >Tokelau</option><option value="TO" >Tonga</option><option value="TT" >Trinidad and Tobago</option><option value="TN" >Tunisia</option><option value="TR" >Turkey</option><option value="TM" >Turkmenistan</option><option value="TC" >Turks and Caicos Islands</option><option value="TV" >Tuvalu</option><option value="UG" >Uganda</option><option value="UA" >Ukraine</option><option value="AE" >United Arab Emirates</option><option value="GB" >United Kingdom (UK)</option><option value="US" >United States (US)</option><option value="UM" >United States (US) Minor Outlying Islands</option><option value="UY" >Uruguay</option><option value="UZ" >Uzbekistan</option><option value="VU" >Vanuatu</option><option value="VA" >Vatican</option><option value="VE" >Venezuela</option><option value="VN" >Vietnam</option><option value="VG" >Virgin Islands (British)</option><option value="VI" >Virgin Islands (US)</option><option value="WF" >Wallis and Futuna</option><option value="EH" >Western Sahara</option><option value="YE" >Yemen</option><option value="ZM" >Zambia</option><option value="ZW" >Zimbabwe</option>
>            </select> 
                </div></div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Street address *</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   <input type="text" class="form-control" placeholder="House Number And Street Name" name="shipping_add1" value="<?php echo $meta["billing_address_1"][0]; ?>">
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <input type="text" class="form-control" placeholder="Apartment, Suite, Unit, etc. (Optional)" name="shipping_add2" value="<?php echo $address_2; ?>">
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Town / City *</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   <input type="text" class="form-control" placeholder="" name="shipping_city">
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Province *</b>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <select class="form-control" name="shipping_province">
            <option value="EC" >Eastern Cape</option><option value="FS" >Free State</option><option value="GP"  selected='selected'>Gauteng</option><option value="KZN" >KwaZulu-Natal</option><option value="LP" >Limpopo</option><option value="MP" >Mpumalanga</option><option value="NC" >Northern Cape</option><option value="NW" >North West</option><option value="WC" >Western Cape</option>
            </select>
            </div></div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Postcode / ZIP *</b>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-12">
                   <input type="text" class="form-control" placeholder="" name="shipping_zip">
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Phone *</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   <input type="text" class="form-control" placeholder="" name="shipping_phone"  value="<?php echo $phone; ?>" >
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Order notes (optional)</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <textarea class="form-control" name="shipping_notes">
                        
                    </textarea>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                   <b>Company Vat Number (optional)</b> 
                </div>
            </div>
            <div class="row"> 
                <div class="col-md-12">
                   <input type="text" class="form-control" placeholder="Vat Number" name="shipping_vat">
                </div>
            </div>
            <br/>
        </div>
    </div>
</div>
<div class="row"> 
<div class="col-md-6">
    &nbsp;
    </div>
                <div class="col-md-6">
                   <input type="submit" value="Place Order" style="margin-left:8px;border-radius: 15px; padding: 10px;border: 1px solid #3A67B1;background-color: #3A67B1; color: #ffffff;width: 100%;font-size: 15px;">
                </div>
            </div>
            <br/><br/><br/>
</form>
<?php
}
else
    {
        ?>
        <br/><br/><br/>
        <table style="width:100%;" class="list-group">
             <tr  class="list-group-item">
                <th colspane="3" valign="middle" align="center">
                    
    						<h4 align="center">Order Created your order Number is : <?php echo $order->id; ?></h4>
    
                </th>
           </tr>
        </table>
        <br/><br/>
        <?php
        
    }
    ?>