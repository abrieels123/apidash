<?php include("admin/includes/header.php"); ?>
        
    <?php //if(!$session->is_signed_in()) { redirect("login.php"); } ?>
    
    <?php
    $user = new User();

if (isset($_POST['create'])) {

  $user_mail                    = 'louise@rightclickmedia.co.za';

  $user_firstname               = $_POST['first_name'];
  $user_surname                 = $_POST['surname'];


  $subject              = "New Installer Registered";
  $style                = 'height:36px;v-text-anchor:middle;width:700px;';
  $style_small          = 'height:36px;v-text-anchor:middle;width:700px;';
  $stylea               = 'color:#ffffff;font-family:Helvetica, Arial,sans-serif;font-size:16px;';
  $style_norm           = 'background-color:#EB7035;border:1px solid #EB7035;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:700px;-webkit-text-size-adjust:none;mso-hide:all;';
  $style_accept           = 'background-color:#37ae50;border:1px solid #37ae50;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:700px;-webkit-text-size-adjust:none;mso-hide:all;';
  $style_reject           = 'background-color:#f72e27;border:1px solid #f72e27;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:340px;-webkit-text-size-adjust:none;mso-hide:all;';
  $outlook_style           = "arcsize='5%' strokecolor='#550062' fillcolor='#550062'"    ;
  $outlook_style_accept    = "arcsize='5%' strokecolor='#37ae50' fillcolor='#37ae50'"    ;
  $outlook_style_reject    = "arcsize='5%' strokecolor='#f72e27' fillcolor='#f72e27'"    ;
    

    $msg                   =  "<table cellpadding='5' cellspacing='5' border='0'>";
    $msg                  .=  "<tr>";
    $msg                  .=  "<th colspan='2'><div style='text-align:center;background-color: #f3f3f3;max-width:700px;color:#550062'><img src='https://rightclickerstesting.co.za/rcm1louise/installers/admin/images/logo.png'><br /><h1 >INSTALLER REGISTERED</h1></div></th>";
    $msg                  .=  "</tr>";
  
    $msg                  .=  "<tr>";
    $msg                  .=  "<td colspan='2'>
    
        <!--[if mso]>
  <v:roundrect xmlns:v='urn:schemas-microsoft-com:vml' xmlns:w='urn:schemas-microsoft-com:office:word' href='https://rightclickerstesting.co.za/rcm1louise/installers/admin/users.php' style='". $style_small . " " .$outlook_style_accept ."' >
    <w:anchorlock/>
    <center style='" . $stylea . "'>VIEW PROFILE</center>
  </v:roundrect>
<![endif]-->

<a href='https://rightclickerstesting.co.za/rcm1louise/installers/admin/users.php' style='" . $style_accept . "'>VIEW PROFILE</a>
    
    
    </td>";
    

    $msg                  .=  "</tr>";
    $msg                  .=  "<tr>";
    $msg                  .=  "<td colspan='2'>";
    $msg                  .=  "<p style='text-align:center'>" . $user_firstname . " " . $user_surname . " Would like to become an installer</p>";
    $msg                  .=  "</td>";
    $msg                  .=  "</tr>";
    $msg                  .=  "<tr>";
    $msg                  .=  "<td colspan='2'><div><a href='https://acdirect.co.za/contact-us/'><img src='https://rightclickerstesting.co.za/rcm1louise/installers/admin/images/footer.png'></a></div></td>";
    $msg                  .=  "</tr>";
    $msg                  .=  "</table>";

  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
  $messages = "<html>
  <head>
  	<title>INSTALLER REGISTERED</title>
  </head>
  <body>
  	
  	<p>".$msg."</p>
  </body>
  </html>";
  if (mail($user_mail, $subject, $messages, $headers)) {
  }else{
   echo "Failed to send email. Please try again later";
  }
} 
    
   if(isset($_POST['create'])) {
       
       if($user) {

       $user->user_username         = $_POST['username'];
       $user->user_firstname        = $_POST['first_name'];
       $user->user_surname          = $_POST['surname'];
       $password                    = $_POST['password'];
       $hashed_password             = password_hash($password, PASSWORD_DEFAULT);
       $user->user_password         = $hashed_password;

       $user->user_contact_number   = $_POST['contact_number'];
       $user->user_email            = $_POST['email'];
       //$user->user_province         = $_POST['province'];
       //$user->user_area             = $_POST['area'];
       //$user->job_limit             = $_POST['job_limit'];
       $user->user_role             = 'client';
       $user->user_rating           = 0;
           
      // $user->set_file($_FILES['user_image']);
       //$user->upload_photo();    
       $user->save();
           
       header('Location: Login.php');
exit;
           
       }

}


?>
<style id="applicationStylesheet" type="text/css">
	.mediaViewInfo {
		--web-view-name: Web 1920 – 6;
		--web-view-id: Web_1920__6;
		--web-scale-on-resize: true;
		--web-enable-deep-linking: true;
	}
	:root {
		--web-view-ids: Web_1920__6;
	}
	* {
		margin: 0;
		padding: 0;
		box-sizing: border-box;
		border: none;
	}
	#Web_1920__6 {
		width: 1920px;
		height: 1080px;
		background-color: rgba(255,255,255,1);
		overflow: hidden;
		--web-view-name: Web 1920 – 6;
		--web-view-id: Web_1920__6;
		--web-scale-on-resize: true;
		--web-enable-deep-linking: true;
	}
	@keyframes fadein {
	
		0% {
			opacity: 0;
		}
		100% {
			opacity: 1;
		}
	
	}
	#Rectangle_4_o {
		fill: url(#Rectangle_4_o);
		stroke: rgba(112,112,112,1);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rectangle_4_o {
		position: absolute;
		overflow: visible;
		width: 1920px;
		height: 1080px;
		left: 0px;
		top: 0px;
	}
	#Rectangle_6_q {
		fill: url(#Rectangle_6_q);
	}
	.Rectangle_6_q {
		width: 1215px;
		height: 6px;
		overflow: visible;
		transform: matrix(1,0,0,1,199.4528,819.3184) rotate(-32deg);
		transform-origin: center;
	}
	#Rectangle_7_s {
		fill: url(#Rectangle_7_s);
	}
	.Rectangle_7_s {
		width: 1215px;
		height: 6px;
		overflow: visible;
		transform: matrix(1,0,0,1,218,849) rotate(-32deg);
		transform-origin: center;
	}
	#Rectangle_8_u {
		fill: url(#Rectangle_8_u);
	}
	.Rectangle_8_u {
		width: 1215px;
		height: 6px;
		overflow: visible;
		transform: matrix(1,0,0,1,236.5472,878.6817) rotate(-32deg);
		transform-origin: center;
	}
	#graphic_api_bot {
		position: absolute;
		width: 1214px;
		height: 1102px;
		left: 725px;
		top: 8px;
		overflow: visible;
	}
	#Rectangle_10 {
		fill: rgba(0,0,0,1);
		stroke: rgba(0,211,255,1);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rectangle_10 {
		filter: drop-shadow(0px 6px 17px rgba(0, 245, 255, 0.502));
		position: absolute;
		overflow: visible;
		width: 275px;
		height: 358px;
		left: 882px;
		top: -80px;
	}
	#Rectangle_24 {
		fill: rgba(255,255,255,1);
	}
	.Rectangle_24 {
		filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
		position: absolute;
		overflow: visible;
		width: 606px;
		height: 1000px;
		left: 700px;
		top: 298px;
	}
	#Rectangle_29 {
		fill: rgba(255,255,255,1);
		stroke: rgba(112,112,112,1);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rectangle_29 {
		overflow: visible;
		width: 436px;
		height: 45px;
		left: 774px;
		top: 492px;
	}
	#Rectangle_26 {
		fill: rgba(255,255,255,1);
		stroke: rgba(112,112,112,1);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rectangle_26 {
		overflow: visible;
		width: 436px;
		height: 45px;
		left: 773px;
		top: 548px;
	}
	#Name {
		left: 789px;
		top: 505px;
		position: absolute;
		overflow: visible;
		width: 400px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(0,0,0,1);
	}
	#surname {
		left: 788px;
		top: 561px;
		position: absolute;
		overflow: visible;
		width: 400px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(0,0,0,1);
    }
    #Password {
		left: 788px;
		top: 610px;
		position: absolute;
		overflow: visible;
		width: 400px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(0,0,0,1);
    }
    #contact_number {
		left: 788px;
		top: 670px;
		position: absolute;
		overflow: visible;
		width: 400px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(0,0,0,1);
    }
    #email {
		left: 788px;
		top: 730px;
		position: absolute;
		overflow: visible;
		width: 400px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(0,0,0,1);
	}
	#Forgot_Password {
		left: 788px;
		top: 604px;
		position: absolute;
		overflow: visible;
		width: 120px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: normal;
		font-size: 13px;
		color: rgba(0,0,0,1);
	}
	#Rectangle_30_ {
		fill: url(#Rectangle_30_);
	}

	#Log_In {
		left: 969px;
		top: 800px;
		position: absolute;
		overflow: visible;
		width: 100px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: bold;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	#Sign_Up {
		left: 969px;
		top: 730px;
		position: absolute;
		overflow: visible;
		width: 100px;
		white-space: nowrap;
		--web-animation: fadein 0.3s ease-out;
		--web-action-type: page;
		--web-action-target: Web_1920___5.html;
		cursor: pointer;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: bold;
		font-size: 16px;
		color: rgba(72,0,83,1);
	}
	#WELCOME_TO_API_BOT {
		left: 758px;
		top: 369px;
		position: absolute;
		overflow: visible;
		width: 472px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: bold;
		font-size: 40px;
		color: rgba(0,0,0,1);
	}
	#API-ROBOT-Recovered {
		position: absolute;
		width: 213px;
		height: 213px;
		left: 886px;
		top: 12px;
		overflow: visible;
	}
</style>
<form action="" method="post" class="form-group" enctype="multipart/form-data">

<div id="Web_1920__6">
	<svg class="Rectangle_4_o">
		<linearGradient id="Rectangle_4_o" spreadMethod="pad" x1="0" x2="0.895" y1="-0.033" y2="0.83">
			<stop offset="0" stop-color="#630072" stop-opacity="1"></stop>
			<stop offset="1" stop-color="#320039" stop-opacity="1"></stop>
		</linearGradient>
		<rect id="Rectangle_4_o" rx="0" ry="0" x="0" y="0" width="1920" height="1080">
		</rect>
	</svg>
	<svg class="Rectangle_6_q">
		<linearGradient id="Rectangle_6_q" spreadMethod="pad" x1="0.067" x2="1" y1="0" y2="0">
			<stop offset="0" stop-color="#84deff" stop-opacity="1"></stop>
			<stop offset="1" stop-color="#ac00c6" stop-opacity="1"></stop>
		</linearGradient>
		<rect id="Rectangle_6_q" rx="0" ry="0" x="0" y="0" width="1215" height="6">
		</rect>
	</svg>
	<svg class="Rectangle_7_s">
		<linearGradient id="Rectangle_7_s" spreadMethod="pad" x1="0.067" x2="1" y1="0" y2="0">
			<stop offset="0" stop-color="#84deff" stop-opacity="1"></stop>
			<stop offset="1" stop-color="#ac00c6" stop-opacity="1"></stop>
		</linearGradient>
		<rect id="Rectangle_7_s" rx="0" ry="0" x="0" y="0" width="1215" height="6">
		</rect>
	</svg>
	<svg class="Rectangle_8_u">
		<linearGradient id="Rectangle_8_u" spreadMethod="pad" x1="0.067" x2="1" y1="0" y2="0">
			<stop offset="0" stop-color="#84deff" stop-opacity="1"></stop>
			<stop offset="1" stop-color="#ac00c6" stop-opacity="1"></stop>
		</linearGradient>
		<rect id="Rectangle_8_u" rx="0" ry="0" x="0" y="0" width="1215" height="6">
		</rect>
	</svg>
	<img id="graphic_api_bot" src="graphic_api_bot.png" srcset="graphic_api_bot.png 1x, graphic_api_bot@2x.png 2x">
		
	<svg class="Rectangle_10">
		<rect id="Rectangle_10" rx="20" ry="20" x="0" y="0" width="224" height="307">
		</rect>
	</svg>
	<svg class="Rectangle_24">
		<rect id="Rectangle_24" rx="30" ry="30" x="0" y="0" width="588" height="700">
		</rect>
	</svg>
	<svg class="Rectangle_29">
		<rect id="Rectangle_29" rx="22.5" ry="22.5" x="0" y="0" width="436" height="45">
		</rect>
	</svg>
	<svg class="Rectangle_26">
		<rect id="Rectangle_26" rx="22.5" ry="22.5" x="0" y="0" width="436" height="45">
		</rect>
	</svg>
	<div id="Name">
    <input  type="text" name="first_name" placeholder="First Name" class="form-control" >

	</div>
	<div id="surname">
    <input  type="text" name="surname" placeholder="Surname" class="form-control" >
	
    </div>
    <div id="password">
    <input  type="password" name="password" placeholder="Password" class="form-control" >
	
    </div>
    <div id="contact_number">
    <input  type="text" name="contact_number" placeholder="Contact Number" class="form-control" >
	
    </div>
    <div id="email">
    <input  type="email" name="email" placeholder="Email" class="form-control" >
	
	</div>
	

	<div id="Log_In">
    <input  type="submit" name="create" class="btn btn-primary pull-right" >
	</div>
	<div id="WELCOME_TO_API_BOT">
		<span>WELCOME TO</span><span style="font-style:normal;font-weight:normal;"> </span><span style="font-style:normal;font-weight:lighter;">API BOT</span>
	</div>
	<img id="API-ROBOT-Recovered" src="API-ROBOT-Recovered.png" srcset="API-ROBOT-Recovered.png 1x, API-ROBOT-Recovered@2x.png 2x">
		
</div>
</form>


