<?php require_once("includes/header.php");
 ?>

<?php


if($session->is_signed_in()) {
    
    //redirect("admin/index.php");
    header("Location: admin/API_Lists.php");
die();
    
}

if(isset($_POST['login'])) {
    
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    
    
    // Mehtod to check db users
    
  $user_found = User::verify_user($username, $password);
//    echo "<script>console.log(" .$user_found . "</script>SHIT)";
    
    
    
    if($user_found) {
//      var_dump($user_found);
        $session->login($user_found);
//                var_dump($user_found);

//           echo '<script>console.log("' . $user_found . '")</script>';
        //redirect("admin/index.php");
        header("Location: admin/API_Lists.php");
    } else {
        
        
        $the_message = "Password or Username is Incorrect";
    }
        
  
} else {
    
    $the_message = "";
    $username = "";
    $password = "";
    
}

?>

<style id="applicationStylesheet" type="text/css">
	.mediaViewInfo {
		--web-view-name: Web 1920 – 6;
		--web-view-id: Web_1920__6;
		--web-scale-on-resize: true;
		--web-enable-deep-linking: true;
	}
	:root {
		--web-view-ids: Web_1920__6;
	}
	* {
		margin: 0;
		padding: 0;
		box-sizing: border-box;
		border: none;
	}
	#Web_1920__6 {
		position: absolute;
		width: 1920px;
		height: 1080px;
		background-color: rgba(255,255,255,1);
		overflow: hidden;
		--web-view-name: Web 1920 – 6;
		--web-view-id: Web_1920__6;
		--web-scale-on-resize: true;
		--web-enable-deep-linking: true;
	}
	@keyframes fadein {
	
		0% {
			opacity: 0;
		}
		100% {
			opacity: 1;
		}
	
	}
	#Rectangle_4_o {
		fill: url(#Rectangle_4_o);
		stroke: rgba(112,112,112,1);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rectangle_4_o {
		position: absolute;
		overflow: visible;
		width: 1920px;
		height: 1080px;
		left: 0px;
		top: 0px;
	}
	#Rectangle_6_q {
		fill: url(#Rectangle_6_q);
	}
	.Rectangle_6_q {
		width: 1215px;
		height: 6px;
		position: absolute;
		overflow: visible;
		transform: matrix(1,0,0,1,199.4528,819.3184) rotate(-32deg);
		transform-origin: center;
	}
	#Rectangle_7_s {
		fill: url(#Rectangle_7_s);
	}
	.Rectangle_7_s {
		width: 1215px;
		height: 6px;
		position: absolute;
		overflow: visible;
		transform: matrix(1,0,0,1,218,849) rotate(-32deg);
		transform-origin: center;
	}
	#Rectangle_8_u {
		fill: url(#Rectangle_8_u);
	}
	.Rectangle_8_u {
		width: 1215px;
		height: 6px;
		position: absolute;
		overflow: visible;
		transform: matrix(1,0,0,1,236.5472,878.6817) rotate(-32deg);
		transform-origin: center;
	}
	#graphic_api_bot {
		position: absolute;
		width: 1214px;
		height: 1102px;
		left: 725px;
		top: 8px;
		overflow: visible;
	}
	#Rectangle_10 {
		fill: rgba(0,0,0,1);
		stroke: rgba(0,211,255,1);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rectangle_10 {
		filter: drop-shadow(0px 6px 17px rgba(0, 245, 255, 0.502));
		position: absolute;
		overflow: visible;
		width: 275px;
		height: 358px;
		left: 882px;
		top: -80px;
	}
	#Rectangle_24 {
		fill: rgba(255,255,255,1);
	}
	.Rectangle_24 {
		filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
		position: absolute;
		overflow: visible;
		width: 606px;
		height: 502px;
		left: 700px;
		top: 298px;
	}
	#Rectangle_29 {
		fill: rgba(255,255,255,1);
		stroke: rgba(112,112,112,1);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rectangle_29 {
		position: absolute;
		overflow: visible;
		width: 436px;
		height: 45px;
		left: 774px;
		top: 492px;
	}
	#Rectangle_26 {
		fill: rgba(255,255,255,1);
		stroke: rgba(112,112,112,1);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rectangle_26 {
		position: absolute;
		overflow: visible;
		width: 436px;
		height: 45px;
		left: 773px;
		top: 548px;
	}
	#Name {
		left: 789px;
		top: 505px;
		position: absolute;
		overflow: visible;
		width: 50px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(0,0,0,1);
	}
	#Password {
		left: 788px;
		top: 561px;
		position: absolute;
		overflow: visible;
		width: 79px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(0,0,0,1);
	}
	#Forgot_Password {
		left: 788px;
		top: 604px;
		position: absolute;
		overflow: visible;
		width: 120px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: normal;
		font-size: 13px;
		color: rgba(0,0,0,1);
	}
	#Rectangle_30_ {
		fill: url(#Rectangle_30_);
	}
	.Rectangle_30_ {
		filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.161));
		position: absolute;
		overflow: visible;
		width: 204px;
		height: 65px;
		left: 901px;
		top: 666px;
		--web-animation: fadein 0.3s ease-out;
		--web-action-type: page;
		--web-action-target: Web_1920___2.html;
		cursor: pointer;
	}
	#Log_In {
		left: 969px;
		top: 679px;
		position: absolute;
		overflow: visible;
		width: 100px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: bold;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	#Sign_Up {
		left: 963px;
		top: 724px;
		position: absolute;
		overflow: visible;
		width: 100px;
		white-space: nowrap;
		--web-animation: fadein 0.3s ease-out;
		--web-action-type: page;
		--web-action-target: Web_1920___5.html;
		cursor: pointer;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: bold;
		font-size: 16px;
		color: rgba(72,0,83,1);
	}
	#WELCOME_TO_API_BOT {
		left: 758px;
		top: 369px;
		position: absolute;
		overflow: visible;
		width: 472px;
		white-space: nowrap;
		text-align: left;
		font-family: Montserrat;
		font-style: normal;
		font-weight: bold;
		font-size: 40px;
		color: rgba(0,0,0,1);
	}
	#API-ROBOT-Recovered {
		position: absolute;
		width: 213px;
		height: 213px;
		left: 886px;
		top: 12px;
		overflow: visible;
	}
</style>
<form action="" method="post" class="form-group" enctype="multipart/form-data">

<div id="Web_1920__6">
	<svg class="Rectangle_4_o">
		<linearGradient id="Rectangle_4_o" spreadMethod="pad" x1="0" x2="0.895" y1="-0.033" y2="0.83">
			<stop offset="0" stop-color="#630072" stop-opacity="1"></stop>
			<stop offset="1" stop-color="#320039" stop-opacity="1"></stop>
		</linearGradient>
		<rect id="Rectangle_4_o" rx="0" ry="0" x="0" y="0" width="1920" height="1080">
		</rect>
	</svg>
	<svg class="Rectangle_6_q">
		<linearGradient id="Rectangle_6_q" spreadMethod="pad" x1="0.067" x2="1" y1="0" y2="0">
			<stop offset="0" stop-color="#84deff" stop-opacity="1"></stop>
			<stop offset="1" stop-color="#ac00c6" stop-opacity="1"></stop>
		</linearGradient>
		<rect id="Rectangle_6_q" rx="0" ry="0" x="0" y="0" width="1215" height="6">
		</rect>
	</svg>
	<svg class="Rectangle_7_s">
		<linearGradient id="Rectangle_7_s" spreadMethod="pad" x1="0.067" x2="1" y1="0" y2="0">
			<stop offset="0" stop-color="#84deff" stop-opacity="1"></stop>
			<stop offset="1" stop-color="#ac00c6" stop-opacity="1"></stop>
		</linearGradient>
		<rect id="Rectangle_7_s" rx="0" ry="0" x="0" y="0" width="1215" height="6">
		</rect>
	</svg>
	<svg class="Rectangle_8_u">
		<linearGradient id="Rectangle_8_u" spreadMethod="pad" x1="0.067" x2="1" y1="0" y2="0">
			<stop offset="0" stop-color="#84deff" stop-opacity="1"></stop>
			<stop offset="1" stop-color="#ac00c6" stop-opacity="1"></stop>
		</linearGradient>
		<rect id="Rectangle_8_u" rx="0" ry="0" x="0" y="0" width="1215" height="6">
		</rect>
	</svg>
	<img id="graphic_api_bot" src="graphic_api_bot.png" srcset="graphic_api_bot.png 1x, graphic_api_bot@2x.png 2x">
		
	<svg class="Rectangle_10">
		<rect id="Rectangle_10" rx="20" ry="20" x="0" y="0" width="224" height="307">
		</rect>
	</svg>
	<svg class="Rectangle_24">
		<rect id="Rectangle_24" rx="30" ry="30" x="0" y="0" width="588" height="484">
		</rect>
	</svg>
	<svg class="Rectangle_29">
		<rect id="Rectangle_29" rx="22.5" ry="22.5" x="0" y="0" width="436" height="45">
		</rect>
	</svg>
	<svg class="Rectangle_26">
		<rect id="Rectangle_26" rx="22.5" ry="22.5" x="0" y="0" width="436" height="45">
		</rect>
	</svg>
	<div id="Name">
    <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo htmlentities($username); ?>" >

	</div>
	<div id="Password">
    <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo htmlentities($password); ?>">
	
	</div>
	<div id="Forgot_Password">
		<span>Forgot Password?</span>
	</div>
	<svg class="Rectangle_30_">
		<linearGradient id="Rectangle_30_" spreadMethod="pad" x1="0.046" x2="0.982" y1="0.5" y2="0.424">
			<stop offset="0" stop-color="#a200ba" stop-opacity="1"></stop>
			<stop offset="1" stop-color="#51005d" stop-opacity="1"></stop>
		</linearGradient>
		<rect onclick="application.goToTargetView(event)" id="Rectangle_30_" rx="23.5" ry="23.5" x="0" y="0" width="186" height="47">
		</rect>
	</svg>
	<div id="Log_In">
    <input type="submit" name="login" value="login" class="btn btn-dark">
	</div>
	<div id="Sign_Up">
    <input type="submit" name="SignUp" value="SignUp" class="btn btn-dark">
	</div>
	<div id="WELCOME_TO_API_BOT">
		<span>WELCOME TO</span><span style="font-style:normal;font-weight:normal;"> </span><span style="font-style:normal;font-weight:lighter;">API BOT</span>
	</div>
	<img id="API-ROBOT-Recovered" src="API-ROBOT-Recovered.png" srcset="API-ROBOT-Recovered.png 1x, API-ROBOT-Recovered@2x.png 2x">
		
</div>
</form>