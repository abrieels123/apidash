<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package thursday
 */


get_header(); ?>
    <div class="search-container">
    <section id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
        <div class="section group products">
	<div class="col span_1_of_4">
<div class="woosidebar">

			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("SIDEBARSEARCH") ) : ?>

	<?php endif;?>
</div>
			 
		
</div>
			<div class="col span_3_of_4">
				<?php if ( have_posts() ) : ?>
 
            <header class="page-header">
                <span class="search-page-title"><?php printf( esc_html__( 'Search Results for: %s', stackstar ), '<span>' . get_search_query() . '</span>' ); ?></span>
            </header><!-- .page-header -->
	
            <?php /* Start the Loop */ ?>
			
			
            <?php while ( have_posts() ) : the_post(); ?>
			<div class="sresult">
				<?php if ( has_post_thumbnail() ): ?>
				<a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail( 'medium', array( 'class' => 'aligncenter' ) ); ?></a>
        <?php endif ?>
				<a href="<?php the_permalink(); ?>"> <span class="search-post-title"><?php the_title(); ?></span></a>
           
           
 		
		</div>
            <?php endwhile; ?>
		
            <?php //the_posts_navigation(); ?>
 
        <?php else : ?>
 
            <?php //get_template_part( 'template-parts/content', 'none' ); ?>
  
        <?php endif; ?>
			</div>
			</div>
 
        
		
			
 
        </main><!-- #main -->
    </section><!-- #primary -->
</div>
</div>
<?php get_footer(); ?>