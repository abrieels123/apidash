<?php include("includes/header.php"); ?>
        
    <?php if(!$session->is_signed_in()) { redirect("../index.php"); } ?>
    
    
    <?php
    
    if(empty($_GET['id'])) {
        
          header("Location: Users.php");
        
    } 

    $TheUser = User::find_by_id($_GET['id']);
    
    if(isset($_POST['update'])) {
        
        if($TheUser) {
            
        $TheUser->user_username            = $_POST['username'];
        $TheUser->user_firstname           = $_POST['first_name'];
        $TheUser->user_surname             = $_POST['surname'];
        $password                          = $_POST['password'];
        $hashed_password                   = password_hash($password, PASSWORD_DEFAULT);
        $TheUser->user_password            = $hashed_password;
        $TheUser->user_contact_number      = $_POST['contact_number'];
        $TheUser->user_email               = $_POST['email'];
        $TheUser->user_branch              = $_POST['branch'];
        $TheUser->user_role                = $_POST['role'];
        $TheUser->user_province            = $_POST['province'];
        $TheUser->user_area                = $_POST['area'];
            
            if(empty($_FILES['user_image'])) {
                
                $TheUser->save();
                
            } else {
                
        $TheUser->set_file($_FILES['user_image']);
        $TheUser->upload_photo(); 
        $TheUser->save();
          header("Location: Users.php");
                
            }  
        } 
}

?>

<?php include "includes/navigation.php" ?> 
            
           
  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
        <!-- Header-->
        <?php include "includes/top-header.php" ?>
        <!-- /#header -->
        <!-- Content -->
        
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           EDIT USER
                        </h1>
                      
                          <div class="col-md-6  offset-md-3">
                              
                              <img class="img-responsive" src="<?php echo $TheUser->image_path_placeholder(); ?>" alt="">
                              
                          </div>
                        <form action="" method="post" class="form-group" enctype="multipart/form-data">
                         <div class="col-md-6  offset-md-3">
                         <div class="form-group">
                          <input type="file" name="user_image">
                            </div>
                           <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" name="username" class="form-control" value="<?php echo $TheUser->user_username ?>" >
                            </div>    
                     
                           <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input  type="text" name="first_name" class="form-control" value="<?php echo $TheUser->user_firstname ?>">
                            </div> 
                              
                           <div class="form-group">
                                <label for="surname">Surname</label>
                                <input  type="text" name="surname" class="form-control" value="<?php echo $TheUser->user_surname ?>">
                            </div>     
                              
                           <div class="form-group">
                                <label for="password">Password</label>
                                <input  type="password" name="password" class="form-control" value="<?php echo $TheUser->user_password ?>">
                            </div>   
                            <div class="form-group">
                                <label for="contact_number">Contact Number</label>
                                <input type="text" name="contact_number" class="form-control" value="<?php echo $TheUser->user_contact_number ?>">
                            </div>  
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input  type="email" name="email" class="form-control" value="<?php echo $TheUser->user_email ?>">
                            </div>  
                            <div class="form-group">
                                <label for="role">User Role</label>
                                <select name="role" id="" class="form-control">
                                    <option value="<?php echo $TheUser->user_role ?>"><?php echo $TheUser->user_role ?></option>
                                    <option value="superadmin">Superadmin</option>
                                       <option value="superadmin">Admin</option>
                                    <option value="agent">Agent</option>
                                </select>
                            </div> 
                            
                           
                            <div class="form-group">
                          
                                <label for="province">Installer Province</label>
                                <select name="province" id="" class="form-control">
                                    <option value="<?php echo $TheUser->user_province ?>"><?php echo $TheUser->user_province ?></option>
                                    <option value="EC">Eastern Cape</option>
                                    <option value="FS">Free State</option>
                                    <option value="GP">Gauteng</option>
                                    <option value="KZN">Kwa-Zulu Natal</option>
                                    <option value="LP">Limpopo</option>
                                    <option value="MP">Mpumalanga</option>
                                    <option value="NC">Northern Cape</option>
                                    <option value="NW">North West</option>
                                    <option value="WC">Western Cape</option>
                                   
                                </select>
                               
                            </div> 
                            
                           <div class="form-group">
                               <a href="delete_user.php?id=<?php echo $TheUser->id ?>" class="btn btn-danger">Delete</a>
                                <input  type="submit" name="update" class="btn btn-dark pull-right" value="UPDATE" >
                            </div>
                            
                                   
                        </div>
                            
                                                                  
                           </form>
                          

                              
                          
                       
                    </div>
                    
                    
                </div>
                <!-- /.row -->

        

  <?php include("includes/footer.php"); ?>