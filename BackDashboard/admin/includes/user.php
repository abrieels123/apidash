<?php

class User extends Db_object {
    
    protected static $db_table = "users";
    protected static $db_table_fields = array('user_status','user_province','user_area','user_username', 'user_password', 'user_firstname', 'user_surname', 'user_contact_number', 'user_email','user_image','user_role');
    public $id;
    public $user_province;
    public $user_area;
    public $user_status;
    public $user_username;
    public $user_password;
    public $user_firstname;
    public $user_surname;
    public $user_contact_number;
    public $user_email;
    public $user_image;
    public $user_role;
    public $upload_directory = 'uploads/graphics';
    public $image_placeholder = 'http://placehold.it/400x400&text=profile_image';
 
    public function image_path_placeholder() {
        
        return empty($this->user_image) ? $this->image_placeholder : $this->upload_directory.DS.$this->user_image;
        
    }  // END IMAGE PLACEHOLDER

    
    public static function verify_user($user_username, $user_password) {
        
    global $database;
    $username = $database->escape_string($user_username);
    $password = $database->escape_string($user_password);

     $sql = "SELECT * FROM "  . self::$db_table . " WHERE ";
     $sql .= "user_username         = '{$username}' ";
//     $sql .= "AND user_status       = 'active' ";
     $sql .="LIMIT 1";

    $the_result_array2 = self::find_by_query($sql);
    $the_result_array = $database->query($sql);
        $result = mysqli_fetch_array($the_result_array);
             $the_password = $result['user_password'];
//            var_dump($the_password) ;

        
    if (password_verify($password, $the_password)) {

     return !empty($the_result_array2) ? array_shift($the_result_array2) : false; 

        } 
    } // END VERIFY USER
    
       public function graphic_path() {
    
        return $this->upload_directory.DS.$this->user_image;
        
    } // END GRAPHIC PATH

        public function delete_user() {
        
        if($this->delete()) {
            
            $target_path = SITE_ROOT . DS . 'admin' . DS . $this->graphic_path();
             
            return unlink($target_path) ? true : false;
            
        } else {
            
            return false;
        }
        
    } // END DELETE
    
   
    public function upload_photo(){
            
            if(!empty($this->errors)) {
                return false;
            }
            
            if(empty($this->user_image) || empty($this->tmp_path)) {
                
                $this->errors[] = "the file was not available";
                return false;
            }
            
            $target_path = SITE_ROOT .DS . 'admin' . DS . $this->upload_directory . DS . $this->user_image;
            
            if(file_exists($target_path)) {
                
                $this->errors[] = "The file {$this->user_image} already exists";
                
                return false;
            }
            
            if(move_uploaded_file($this->tmp_path, $target_path)) {
                
//                if( $this->create()) {
                    
                    unset($this->tmp_path);
                    return true;
                    
//                }
                
            } else {
                
                
                $this->error[] = "Error: Check folder permissions";
                
                return false;
            }
            
            $this->create();
//        }
        
    } // END UPLOAD PHOTO

    public static function find_current_user() { 
        global $database;
        $users_id = $_SESSION['id'];   
        return static::find_by_query("SELECT * FROM "  . static::$db_table . " WHERE id = " . $users_id . " ");
            
        }    // END FIND CURRENT

    public static function find_all_role() { 
        global $database;
        $users_id = $_SESSION['id'];   
        return static::find_by_query("SELECT user_role FROM "  . static::$db_table . " WHERE id = " . $users_id . " ");
            
        }    // END FIND ALL


    public static function find_user_province() { 
        global $database;
        $users_province = $_GET['province'];
        $role = 'installer';
        return static::find_by_query("SELECT * FROM users WHERE user_province =  '{$users_province}' AND user_role = '{$role}'  " );
            
        }    // END FIND ALL
    
    
//    public static function  max_rating() { 
//        global $database;
//        $role = 'installer';
//        return static::find_by_query("SELECT users.id, MAX(user_rating) as user_rating FROM users, jobs WHERE user_province = 'GP' AND job_limit < " . " (SELECT count(*) FROM jobs WHERE jobs.installer = users.id and install_date = '2020-08-10') " . " GROUP BY users.id LIMIT 1");
//        
//            
//        }    // END FIND ALL

    public function change_user_status() { 
    
        if(isset($_GET['approve'])) {
            global $database;
            $the_id = $_GET['approve'];
            $the_date = date("Y/m/d");
            $query = "UPDATE users SET user_status = 'active', date_changed = '$the_date' WHERE id = $the_id ";
            $database->query($query);

            return (mysqli_affected_rows($database->connection) == 1) ? true : false;

        }

        if(isset($_GET['unapprove'])) {
            global $database;
            $the_id = $_GET['unapprove'];
            $the_date = date("Y/m/d");
            $query = "UPDATE users SET user_status = 'inactive', date_changed = '$the_date' WHERE id = $the_id ";
            $database->query($query);

            return (mysqli_affected_rows($database->connection) == 1) ? true : false;

        } 
                    
        }    // END CHANGE STATUS
    
        // FIND INSTALLATIONS THAT ARE TWO DAYS AWAY FROM CURRENT DATE
        public static function find_user_email_for_reminder() { 

        global $database;
        $the_date = date("Y/m/d");
        $TwoDaysAhead = date('Y/m/d',strtotime("+2 days"));
            
        return static::find_by_query("SELECT user_email, id FROM users WHERE id IN " . "(SELECT installer FROM jobs WHERE install_date = '$TwoDaysAhead' GROUP BY id )" .  " " );

                    
        }    // END EMAIL REMINDER 2 DAYS FROM NOW  
    
        public static function on_day_reminder() { 

        global $database;
        $the_date = date("Y/m/d");
        
        return static::find_by_query("SELECT user_email, id FROM users WHERE id IN " . "(SELECT installer FROM jobs WHERE install_date = '$the_date' GROUP BY id )" .  " " );

                    
        }    // END EMAIL REMINDER 2 DAYS FROM NOW 
    
        public static function find_installers_email($installer) { 

        global $database;
        $the_installer = $installer;
            
        return static::find_by_query("SELECT * FROM users WHERE id = '$the_installer' LIMIT 1");

//        return !empty($the_result_array) ? array_shift($the_result_array) : false; 

        } // FIND INSTALLERS EMAIL 
    
        public static function find_jobs_by_user_id($the_id) { 

        global $database;
        $user = $the_id;
            
        return static::find_by_query("SELECT user_email FROM users WHERE id = '$user' LIMIT 1" );

        }    // FIND JOB EMAIL BY ID  
    
        // FIND INSTALLATIONS THAT TIME HAS NOT BEEN SET
        public static function set_time_reminder() { 

        global $database;

        return static::find_by_query("SELECT user_email, t1.id FROM users t1 left join jobs t2 on t1.id = t2.installer WHERE t1.id IN " . "(SELECT installer FROM jobs WHERE purchase_date < CURRENT_DATE() AND install_time is NULL GROUP BY t1.id )" .  " " );
                    
        }    // END EMAIL TIME REMINDER 
    
        public static function find_user_email_completed_reminder() { 

            global $database;
            $the_date = date("Y/m/d");
            $TwoDaysAhead = date('Y/m/d',strtotime("+2 days"));

        return static::find_by_query("SELECT user_email, id FROM users WHERE id IN " . "(SELECT installer FROM jobs WHERE install_date < CURRENT_DATE() GROUP BY id )" .  " " );
          
        }    // END COMPLETE JOB  
    
            
        public static function find_installer_email($job_id) {       
        global $database;
            
        $the_order = $_GET['reschedule'];
           
            return static::find_by_query("SELECT user_email FROM users WHERE id = " . "(SELECT installer FROM jobs WHERE order_nr = '$job_id' GROUP BY id LIMIT 1)" .  " LIMIT 1" );

        } // END FIND OPEN JOBS
    
        public static function find_next_rated_user($province, $date) {
        
        $role           = "installer";
        $status         = "active";
        $province       = $province;
        $delivery_date  = $date;
        $user           = $_SESSION['id'];
        $GLOBALS["Date"] = "";
        global $database;

        $the_result_array = static::find_by_query("SELECT t1.id, t1.user_email ,max(user_rating) as user_rating FROM users t1 left join jobs t2 on t1.id = t2.installer WHERE (NOT t1.id = '$user') AND user_province = '$province' AND user_role = '$role' AND user_status = '$status' AND job_limit > (SELECT count(*) FROM jobs t3 WHERE t3.installer = t1.id and t3.install_date = '$delivery_date') group by t1.id order by 3 desc LIMIT 1");
  
        
         if (!empty($the_result_array)) {
            return array_shift($the_result_array) ; 
             
         }
        $day_count = 1;
        if (empty($the_result_array)) { 
            
            $validation_check = 0;

              while(($validation_check <= 0) AND ($day_count <= 14)){
        
                $_POST['startdate']     = $delivery_date;
                $_POST['numberofdays']  = $day_count;
                $d                      = new DateTime( $_POST['startdate'] );
                $t                      = $d->getTimestamp();
                // loop for X days
                for($i=0; $i<$_POST['numberofdays']; $i++){
                // add 1 day to timestamp
                    $addDay = 86400;
                    // get what day it is next day
                    $nextDay = date('w', ($t+$addDay));
                    // if it's Saturday or Sunday get $i-1
                if($nextDay == 0 || $nextDay == 6) {
                    $i--;
            }
            // modify timestamp, add 1 day
                $t = $t+$addDay;
            }

            $d->setTimestamp($t);

            $delivery_date_new  = $d->format( 'Y-m-d' );

             $the_result_array2 = static::find_by_query("SELECT t1.id,  t1.user_email ,max(user_rating) as user_rating FROM users t1 left join jobs t2 on t1.id = t2.installer WHERE (NOT t1.id = '$user') AND user_province = '$province' AND user_role = '$role' AND user_status = '$status' AND job_limit > (SELECT count(*) FROM jobs t3 WHERE t3.installer = t1.id and t3.install_date = '$delivery_date_new') group by t1.id order by 3 desc LIMIT 1");

                  if(!empty($the_result_array2)) {

                    $GLOBALS["Date"]    = $delivery_date_new;
                    $validation_check   = 1;
                    return array_shift($the_result_array2) ;
                    
                  }
            
                   $day_count++;
                  
              }

        } // END IF 
          
        if (empty($the_result_array2)) {

            redirect("no_installers.php");
            return;
            
        }

    } // END FIND NEXT RATED USER
    
    public static function find_next_open_date($date) {
        
        $role           = "installer";
        $status         = "active";
        $province       = $province;
        $delivery_date  = $date;
        $user           = $_SESSION['id'];
        $GLOBALS["Date"] = "";
        global $database;

        $the_result_array = static::find_by_query("SELECT t1.id, t1.user_email ,max(user_rating) as user_rating FROM users t1 left join jobs t2 on t1.id = t2.installer WHERE (NOT t1.id = '$user') AND user_province = '$province' AND user_role = '$role' AND user_status = '$status' AND job_limit > (SELECT count(*) FROM jobs t3 WHERE t3.installer = t1.id and t3.install_date = '$delivery_date') group by t1.id order by 3 desc LIMIT 1");
  
        $day_count = 1;
        
        if (empty($the_result_array)) { 
            
            $validation_check = 0;

              while($validation_check <= 0){
        
                $_POST['startdate']     = $delivery_date;
                $_POST['numberofdays']  = $day_count;
                $d                      = new DateTime( $_POST['startdate'] );
                $t                      = $d->getTimestamp();
                // loop for X days
                for($i=0; $i<$_POST['numberofdays']; $i++){
                // add 1 day to timestamp
                    $addDay = 86400;
                    // get what day it is next day
                    $nextDay = date('w', ($t+$addDay));
                    // if it's Saturday or Sunday get $i-1
                if($nextDay == 0 || $nextDay == 6) {
                    $i--;
            }
            // modify timestamp, add 1 day
                $t = $t+$addDay;
            }

            $d->setTimestamp($t);

            $delivery_date_new  = $d->format( 'Y-m-d' );

             $the_result_array2 = static::find_by_query("SELECT t1.id,  t1.user_email ,max(user_rating) as user_rating FROM users t1 left join jobs t2 on t1.id = t2.installer WHERE (NOT t1.id = '$user') AND user_province = '$province' AND user_role = '$role' AND user_status = '$status' AND job_limit > (SELECT count(*) FROM jobs t3 WHERE t3.installer = t1.id and t3.install_date = '$delivery_date_new') group by t1.id order by 3 desc LIMIT 1");

                  if(!empty($the_result_array2)) {

                    $GLOBALS["Date"]    = $delivery_date_new;
                    $validation_check   = 1;
                    return array_shift($the_result_array2) ;
                    
                  }
            
                   $day_count++;
              }

        } // END IF 
        else {
            return array_shift($the_result_array) ; 
        }

    } // END FIND NEXT AVAILABLE DATE

} // End of User Class 

?>