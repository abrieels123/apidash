<?php

class Db_object {
    
    protected static $db_table = "users";
    
    public static function find_all() {    
    return static::find_by_query("SELECT * FROM "  . static::$db_table . " ");
        
    }    // END FIND ALL

    
    public static function find_by_id($id) {       
    global $database;
    $the_result_array = static::find_by_query("SELECT * FROM "  . static::$db_table . " WHERE id = $id LIMIT 1");

    return !empty($the_result_array) ? array_shift($the_result_array) : false;    

    } // END FIND BY ID
    
    public function set_file($file) {
        
    if(empty($file) || !$file || !is_array($file)) {

        $this->errors[] = "There was no file uploaded here";
        return false;

    } elseif($file['error'] !=0) {

        $this->errors[] = $this->upload_errors_array[$file['error']];
        return false;

    } else {

    $this->user_image   = basename($file['name']);
    $this->tmp_path     = $file['tmp_name'];
    $this->graphic_type = $file['type'];
    $this->graphic_size = $file['size'];

    }
        
 
    } // END SET FILE

        

        
    public static function find_by_query($sql) {
    global $database;
    $result_set = $database->query($sql);
    $the_object_array = array();
        
        while($row =  mysqli_fetch_array($result_set)) {
            
            $the_object_array[] = static::instantiation($row);

        }
            
    return $the_object_array;

    } // END FIND THIS Q
    
        public static function find_by_single_query($sql) {
    global $database;
    $result_set = $database->query($sql);
    
    $row =  mysqli_fetch_assoc($result_set);
            
    return  $row;

    } // END FIND THIS Q
    
        public static function instantiation($the_record){
            
            $calling_class = get_called_class();
        
    $the_object = new $calling_class;
        
    foreach ($the_record as $the_attribute => $value) {
        
    if($the_object->has_the_attribute($the_attribute)) {
        
    $the_object->$the_attribute = $value;
        
        }
    }

    return $the_object;
            
} // END INSTANTIATION
    
        protected function properties() {
                
        
        $properties = array();
        
        foreach (static::$db_table_fields as $db_field) {
            
            if(property_exists($this, $db_field)) {
                
                $properties[$db_field] = $this->$db_field;            
            }
        }
        
        return $properties;
        
    } // END PROPERTIES
    
        private function has_the_attribute($the_attribute) {
        
    $object_properties = get_object_vars($this);
        
    return array_key_exists($the_attribute, $object_properties);

} // END HAS ATTRIBUTE
    
        protected function clean_properties(){
        
        
        global $database;
        
        $clean_properties = array();
        
        foreach ($this->properties() as $key => $value) {
            
            $clean_properties[$key] = $database->escape_string($value);
        }
        
         return $clean_properties;
        
    } // END CLEAN PROPERTIES
    
    
     public function save() {
        
        return isset($this->id) ? $this->update() : $this->create();
        
    }
    

    public function create() {
        
        $properties = $this->clean_properties();
        
        global $database;
        
        $sql = "INSERT INTO " . static::$db_table . "(" .  implode(",", array_keys($properties))  . ")";
        $sql .= "VALUES ('" .  implode("','", array_values($properties))      . "')";

        
        if($database->query($sql)) {
            
            $this->id = $database->the_insert_id();
            
            return true;
            
        } else {
            
            return false;
            
        }
        
        
    } // END CREATE
    
    
    public function update() {
                
        global $database;
        
        $properties = $this->clean_properties();
        
        $properties_pairs = array();
        
        foreach ($properties as $key => $value) {
            
            $properties_pairs[] = "{$key}='{$value}'";
        }
        
        $sql = "UPDATE " . static::$db_table . " SET ";
        $sql .= implode(", ", $properties_pairs);
        $sql .= " WHERE id = " . $database->escape_string($this->id);
        
        
        $database->query($sql);
        
        
        return (mysqli_affected_rows($database->connection) == 1) ? true : false;
        
    } // END UPDATE
    
        public function delete() {
          
          global $database;
          $sql = "DELETE FROM " . static::$db_table . " ";
          $sql .= "WHERE id=" . $database->escape_string($this->id);
          $sql .= " LIMIT 1";
          
          $database->query($sql);
          
          return (mysqli_affected_rows($database->connection) == 1) ? true : false;

          
          
          
      } // END DELETE
    public static function count_by_query($sql) {
    global $database;
    $result_set = $database->query($sql);
//    $the_object_array = array();
        
    $row =  mysqli_num_rows($result_set);
            
    return  $row;

    } // END FIND THIS Q
    
    
    public static function count_all() {
        
        global $database;
        
        $sql = "SELECT COUNT(*) FROM " . static::$db_table;
        $result_set = $database->query($sql);
        $row = mysqli_fetch_array($result_set);
        
        return array_shift($row);
        
        
        
        
    }
    
    
} // END CLASS

?>