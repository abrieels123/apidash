<?php

    class Invoice extends Db_object {

        protected static $db_table = "jobs";
        protected static $db_table_fields = array('order_nr','name', 'surname', 'province', 'area', 'street', 'phone','email','install_date','install_time','installer','status', 'purchase_date', 'Confirmed_Amount','Confirmed','Invoice_amount');
        public $id;
        public $order_nr;
        public $name;
        public $surname;
        public $province;
        public $area;
        public $street;
        public $phone;
        public $email;
        public $install_date;
        public $install_time;
        public $installer;
        public $status;
        public $purchase_date;
        public $Invoice_amount;
        public $Confirmed_Amount;
        public $Confirmed;
        public $upload_directory    = 'uploads/graphics';
        public $image_placeholder   = 'http://placehold.it/400x400&text=profile_image';
        
        public function delete_appointment() {

        if($this->delete()) {

        } else {

        return false;
            
        }

        } // END DELETE

        public static function find_current_appointment() { 
        global $database;

        return static::find_by_query("SELECT * FROM appointments WHERE install_date = '{$the_date}' ORDER BY install_time");

        }    // END FIND APPOINTMENT

        public static function find_by_order_id() { 
        global $database;

        $order =  $_GET['order'];

        return static::find_by_query("SELECT * FROM jobs WHERE order_nr = '{$order}' LIMIT 1");

        }    // END FIND APPOINTMENT
        
        public static function find_by_order_id2() { 
        global $database;

        $order =  $_GET['complete'];

        return static::find_by_query("SELECT * FROM jobs WHERE order_nr = '{$order}' ");

        }    // END FIND APPOINTMENT

        public static function find_job_id($the_id) {       
        global $database;
        $job_id = $the_id;
        $the_result_array = static::find_by_query("SELECT * FROM jobs WHERE order_nr = '$job_id' LIMIT 1");

        return !empty($the_result_array) ? array_shift($the_result_array) : false;    

        } // END FIND BY ID

        public static function find_open_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT * FROM jobs WHERE installer = 'unassigned' AND status = 'pending' AND blocked != $user AND province = " . "(SELECT user_province FROM users WHERE id = $user )" .  " " );

        } // END FIND OPEN JOBS

        public static function find_completed_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT * FROM jobs WHERE installer =  $user AND status = 'completed'  ");    

        } // END FIND COMPLETED JOBS
        
        
        public static function find_all_active_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT * FROM jobs WHERE status = 'pending' OR status = 'accepted'");    

        } // END FIND ACTIVE JOBS
        
        public static function find_all_completed_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT * FROM jobs WHERE status = 'completed'  ");    

        } // END FIND COMPLETED JOBS
        
       public static function find_Confirmed_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT distinct j.* ,price as 'Invoice_amount', case when Confirmed = 0 then 'false' else 'true' end as 'Confirmed'
                                        FROM `completed_jobs` cj
                                        inner join jobs j on j.order_nr = cj.order_nr  WHERE j.installer =  $user AND status = 'completed'  ");    

        } // END FIND confirmed JOBS
        
        public static function find_all_Confirmed_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT distinct j.* ,price as 'Invoice_amount', case when Confirmed = 0 then 'false' else 'true' end as 'Confirmed'
                                        FROM `completed_jobs` cj
                                        inner join jobs j on j.order_nr = cj.order_nr WHERE status = 'completed'  ");    

        } // END FIND confirmed JOBS
        
        public static function find_Paid_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT  distinct j.* ,price as 'Invoice_amount'
                                        FROM `completed_jobs` cj
                                        inner join jobs j on j.order_nr = cj.order_nr WHERE j.installer =  $user AND status = 'confirmed'  ");    

        } // END FIND Paid JOBS
        
        public static function find_all_Paid_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT  distinct j.* ,price as 'Invoice_amount'
                                        FROM `completed_jobs` cj
                                        inner join jobs j on j.order_nr = cj.order_nr WHERE status = 'confirmed'  ");    

        } // END FIND Paid JOBS
        
        public static function find_unassigned_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT * FROM jobs WHERE installer = '0'  ");    

        } // END FIND UNASSIGNED JOBS
        
        public static function find_rescheduled_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT * FROM jobs WHERE installer =  $user AND status = 'rescheduled'  ");    

        } // END FIND RESCHEDULED JOBS    
        
        public static function find_all_rescheduled_jobs() {       
        global $database;

        return static::find_by_query("SELECT * FROM jobs WHERE status = 'rescheduled'  ");    

        } // END FIND RESCHEDULED JOBS   
        
        public static function find_pending_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT * FROM jobs WHERE installer =  $user AND status = 'pending'  ");    

        } // END FIND PENDING JOBS    
        
        public static function find_all_pending_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT * FROM jobs WHERE status = 'pending'  ");    

        } // END FIND PENDING JOBS   
        
        public static function find_delivered_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT * FROM jobs WHERE installer =  $user AND status = 'delivered'  ");    

        } // END FIND ACCEPTED JOBS          
        
        
        public static function find_received_jobs() {       
        global $database;

        return static::find_by_query("SELECT * FROM jobs WHERE status = 'received'  ");    

        } // END FIND RECEIVED JOBS   
        
        public static function find_all_delivered_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        return static::find_by_query("SELECT * FROM jobs WHERE status = 'delivered'  ");    

        } // END FIND ACCEPTED JOBS
        
        public static function find_users_jobs() {       
        global $database;
        $time = '00:00:00';
        $user = $_SESSION['id'];
        return static::find_by_query("SELECT * FROM jobs WHERE installer =  '$user' AND status != 'completed' AND install_time IS NOT NULL AND install_time != '$time'");    

        } // END FIND COMPLETED JOBS    
        
        public static function find_all_users_jobs($the_id) {       
        global $database;
        $user = $the_id;
        return static::find_by_query("SELECT * FROM jobs WHERE installer = '$user' AND status != 'completed'");    

        } // END FIND COMPLETED JOBS 
        
        public static function find_on_day_jobs($the_id) {       
        global $database;
        $user           = $the_id;
        $the_date       = date("Y/m/d");
        return static::find_by_query("SELECT * FROM jobs WHERE installer = '$user' AND status != 'completed' AND install_date = '$the_date'");    

        } // END FIND COMPLETED JOBS         
        
        public static function find_2_days_ahead_jobs($the_id) {       
        global $database;
        $user           = $the_id;
        $the_date       = date("Y/m/d");
        $TwoDaysAhead = date('Y/m/d',strtotime("+2 days"));
        return static::find_by_query("SELECT * FROM jobs WHERE installer = '$user' AND status != 'completed' AND install_date = '$TwoDaysAhead'");    

        } // END FIND COMPLETED JOBS   
        
        public static function find_no_time_set_jobs($the_id) {       
        global $database;
        $user           = $the_id;
      
        return static::find_by_query("SELECT * FROM jobs WHERE installer = '$user' AND status != 'completed' AND purchase_date < CURRENT_DATE()");    

        } // END FIND COMPLETED JOBS 
        
        public static function find_latest_users_jobs() {       
        global $database;

        $user = $_SESSION['id'];
        $time = '00:00:00';
        return static::find_by_query("SELECT * FROM jobs WHERE installer =  $user AND status != 'completed' AND (install_time IS NULL OR install_time = '$time')");    

        } // END FIND COMPLETED JOBS
        
        public function Update_Jobs_Totals($amount,$orderno)
        {
           if($orderno >0)
           {
                global $database;
                $Confirmed_amount = $amount;
                $order = $orderno;
                $query = "UPDATE jobs SET status = 'confirmed',Confirmed_amount = ".$Confirmed_amount.",confirmed = '1' WHERE order_nr = $order ";
                $database->query($query);
                //echo $query;
                return (mysqli_affected_rows($database->connection) == 1) ? true : false;
           }
        }
        
        public function complete_job() { 
    
            if(isset($_GET['order'])) {
                
                global $database;
                $completed = $_GET['order'];

                $query = "UPDATE jobs SET status = 'completed' WHERE order_nr = $completed ";
                $database->query($query);

                return (mysqli_affected_rows($database->connection) == 1) ? true : false;

            } // END IF
    
        }    // END COMPLETE JOB         
        
        public function admin_accept_installer() { 
    
            if(isset($_GET['order'])) {
                
                global $database;
                $accepted = $_GET['order'];

                $query = "UPDATE jobs SET status = 'pending' WHERE order_nr = '$accepted' ";
                $database->query($query);

                return (mysqli_affected_rows($database->connection) == 1) ? true : false;

            } // END IF
    
        }    // END COMPLETE JOB  
        
        public function aircon_delivered() { 
    
            if(isset($_GET['order'])) {
                
                global $database;
                $completed = $_GET['order'];

                $query = "UPDATE jobs SET status = 'delivered' WHERE order_nr = $completed ";
                $database->query($query);

                return (mysqli_affected_rows($database->connection) == 1) ? true : false;

            } // END IF
         
        }    // END COMPLETE JOB  
        
        public function request_new_installer() { 

        global $database;
        $installer_id = $_GET['id'];
        $order_nr     = $_GET['order'];
            
        $query = "UPDATE jobs SET status = 'incomplete', installer = 'unassigned', blocked = $installer_id WHERE order_nr = $order_nr ";
        $database->query($query);

        return (mysqli_affected_rows($database->connection) == 1) ? true : false;
        
        }    // END REQUEST NEW INSTALLER
        
        public function installer_accepted() { 
    
            if(isset($_GET['accepted'])) {
                global $database;
                $accepted = $_GET['accepted'];

                $query = "UPDATE jobs SET status = 'accepted' WHERE order_nr = $accepted ";
                $database->query($query);

                return (mysqli_affected_rows($database->connection) == 1) ? true : false;
            } // END IF
                    
        }    // END CHANGE STATUS
        
        public static function find_user_delivery_date() { 
    
        global $database;
        $the_date = date("Y/m/d");
        $TwoDaysAhead = date('Y/m/d',strtotime("+2 days"));

        return static::find_by_query("SELECT email,install_date FROM jobs WHERE install_date = '$TwoDaysAhead' GROUP BY id ");

        }    // END COMPLETE JOB     
        
        public static function find_user_delivery_not_confirmed() { 
    
            global $database;
            $the_date       = date("Y/m/d");
            $ThreeDays      = date('Y/m/d',strtotime("+3 days"));


        return static::find_by_query("SELECT * FROM jobs WHERE (status = 'pending' OR status = 'accepted' OR status = 'rescheduled')  AND install_date = '$ThreeDays' GROUP BY id ");
   
        }    // END FIND UNDELIVERED INSTALLATIONS 
        
        public static function check_date_job_limit($date, $user_id) {
            
            global $database;
            $delivery_date      = $date;
            $installer          = $user_id;
            $GLOBALS["Date"]    = " ";
                        
            $the_result_array = static::find_by_query("SELECT t2.id, t2.install_date FROM users t1 left join jobs t2 on t1.id = t2.installer WHERE t1.id = '$installer' AND job_limit > (SELECT count(*) FROM jobs t3 WHERE t3.installer = t1.id and t3.install_date = '$delivery_date')");
                              
            $day_count = 1;

            if (empty($the_result_array)) { 

                $validation_check = 0;

                  while($validation_check <= 0){

                    $_POST['startdate']     = $delivery_date;
                    $_POST['numberofdays']  = $day_count;
                    $d                      = new DateTime( $_POST['startdate'] );
                    $t                      = $d->getTimestamp();
                    // loop for X days
                    for($i=0; $i<$_POST['numberofdays']; $i++){
                    // add 1 day to timestamp
                        $addDay = 86400;
                        // get what day it is next day
                        $nextDay = date('w', ($t+$addDay));
                        // if it's Saturday or Sunday get $i-1
                    if($nextDay == 0 || $nextDay == 6) {
                        $i--;
                }
                // modify timestamp, add 1 day
                    $t = $t+$addDay;
                }

                $d->setTimestamp($t);

                $delivery_date_new  = $d->format( 'Y-m-d' );

                $the_result_array2 = static::find_by_query("SELECT t2.id, t2.install_date FROM users t1 left join jobs t2 on t1.id = t2.installer WHERE t1.id = '$installer' AND job_limit > (SELECT count(*) FROM jobs t3 WHERE t3.installer = t1.id and t3.install_date = '$delivery_date_new')");

                  if(!empty($the_result_array2)) {

                    $GLOBALS["Date"]    = $delivery_date_new;
                    $validation_check   = 1;
                    return array_shift($the_result_array2) ;

                  }

                   $day_count++;
                  } // ENd WHILE

            } // END IF
                else {
                $GLOBALS["Date"] = $delivery_date;
                return array_shift($the_result_array) ; 
            }
             
         } // END CHECK IF DATE IS OPEN
        
        
        public static function find_user_completed_job() { 
    
        global $database;
        $the_date = date("Y/m/d");
        $TwoDaysAgo = date('Y/m/d',strtotime("-2 days"));

        return static::find_by_query("SELECT email,install_date FROM jobs WHERE install_date = '$TwoDaysAgo' AND status != 'completed' GROUP BY id ");
           
        }    // END COMPLETE JOB  
        
        public function count_completed_jobs() {       
        global $database;
        $the_id = $_SESSION['id'];
        return static::count_by_query("SELECT * FROM jobs WHERE status = 'completed' AND installer = $the_id");
   
        } // END COUNT COMPLETED JOBS        
        
        public function count_all_completed_jobs() {       
        global $database;
        $the_id = $_SESSION['id'];
        return static::count_by_query("SELECT * FROM jobs WHERE status = 'completed'");
   
        } // END COUNT COMPLETED JOBS
        
        public function count_delivered_jobs() {       
        global $database;
        $the_id = $_SESSION['id'];
        return static::count_by_query("SELECT * FROM jobs WHERE status = 'delivered' AND installer = $the_id");
   
        } // END COUNT COMPLETED DELIVERED     
        
        public function count_all_delivered_jobs() {       
        global $database;
        $the_id = $_SESSION['id'];
        return static::count_by_query("SELECT * FROM jobs WHERE status = 'delivered' ");
   
        } // END COUNT COMPLETED DELIVERED   
        
        public function count_rescheduled_jobs() {       
        global $database;
        $the_id = $_SESSION['id'];
        return static::count_by_query("SELECT * FROM jobs WHERE status = 'rescheduled' AND installer = $the_id");
   
        } // END COUNT COMPLETED RESCHEDULED   
        
        public function count_all_rescheduled_jobs() {       
        global $database;
        $the_id = $_SESSION['id'];
        return static::count_by_query("SELECT * FROM jobs WHERE status = 'rescheduled' ");
   
        } // END COUNT COMPLETED RESCHEDULED   
        
        public function count_pending_jobs() {       
        global $database;
        $the_id = $_SESSION['id'];
        return static::count_by_query("SELECT * FROM jobs WHERE status = 'pending' AND installer = $the_id");

        } // END COUNT COMPLETED PENDING   
        
        public function count_all_pending_jobs() {       
        global $database;
        $the_id = $_SESSION['id'];
        return static::count_by_query("SELECT * FROM jobs WHERE status = 'pending'");
   
        } // END COUNT COMPLETED PENDING

        public static function on_day_client_reminder() { 

        global $database;
        $the_date = date("Y/m/d");
        
        return static::find_by_query("SELECT email FROM jobs WHERE install_date = '$the_date'" );

                    
        }    // END EMAIL REMINDER 2 DAYS FROM NOW 
        
            
        // FIND INSTALLATIONS THAT ARE TWO DAYS AWAY FROM CURRENT DATE
        public static function find_client_email_for_reminder() { 

        global $database;
        $the_date = date("Y/m/d");
        $TwoDaysAhead = date('Y/m/d',strtotime("+2 days"));
            
        return static::find_by_query("SELECT email, install_date, order_nr FROM jobs WHERE install_date = '$TwoDaysAhead'" );

                    
        }    // END EMAIL REMINDER 2 DAYS FROM NOW 


    } // End of JOBS Class

?>