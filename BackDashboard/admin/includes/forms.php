<?php

class Forms extends Db_object {

    public function Gender(){
    return '<p><label for="gender">Gender</label><select class="form-control" name="camp_gender" required><option value="Male">Male</option><option value="Female">Female</option><option value="Other">Other</option></select></p>';
 }   // END GENDER
 
    public function AgeGroups(){
    return '<p><label for="age">Age Group</label><select class="form-control" name="camp_age" required><option value="18-24">18-24</option><option value="25-34">25-34</option><option value="35-44">35-44</option><option value="45-54">45-54</option><option value="55-64">55-64</option><option value="65 or more">65 or more</option><option value="Unknown">Unknown</option></select></p>';
 } // END AGE

    public function ParentalStatus(){
    return '<p><label for="parental">Parental Status</label><select class="form-control" name="camp_parental_status" required><option value="Yes">Yes</option><option value="No">No</option></select></p>';
}   // END PARENTAL STATS

public function HomeOwner(){
   return '<p><label for="homeowner">Home Owner</label><select class="form-control" name="home_owner" required><option value="Yes">Yes</option><option value="No">No</option></select></p>';
}   // END HOME OWNER



} // END CLASS
