<?php
$UserRole = User::find_all_role();

$userId =  $_SESSION['id'];
?>

<?php foreach ($UserRole as $role) : ?>
<?php endforeach; ?>
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dashboardmenu">
                    <a href="index.php" data-title='Dashboard'><i class="menu-icon fa fa-laptop tool"></i>Dashboard </a>
                </li>

                <?php
                $userrole = $role->user_role;
                if ($userrole == 'superadmin' || $userrole == 'admin') {
                    echo "<li class='calendarmenu'><a href='API_Lists.php' data-title='API Lists'><i class='fas fa-calendar-alt menu-icon' title='API Lists'></i> API LISTS </a></li>";
                    echo "<li class='calendarmenu'><a href='my_profile.php' data-title='MyProfile'><i class='fas fa-calendar-alt menu-icon' title='MyProfile'></i> MYPROFILE</a></li>";
                    echo "<li class='calendarmenu'><a href='createapi.php' data-title='CREATE NEW API'><i class='fas fa-calendar-alt menu-icon' title='CREATE NEW API'></i> CREATE NEW API</a></li>";
                    echo "<li class='calendarmenu'><a href='subscription.php' data-title='SUBSCRIPTION'><i class='fas fa-calendar-alt menu-icon' title='SUBSCRIPTION'></i> SUBSCRIPTION</a></li>";
                    echo "<li class='calendarmenu'><a href='Logout.php' data-title='Logout'><i class='fas fa-calendar-alt menu-icon' title='Calendar'></i> Logout</a></li>";
                }
                ?>
                <?php
                $userrole = $role->user_role;
                if ($userrole == 'agent') {
                    echo "<li class='calendarmenu'><a href='API_Lists.php' data-title='API Lists'><i class='fas fa-calendar-alt menu-icon' title='API Lists'></i> API LISTS </a></li>";
                    echo "<li class='calendarmenu'><a href='my_profile.php' data-title='MyProfile'><i class='fas fa-calendar-alt menu-icon' title='MyProfile'></i> MYPROFILE</a></li>";
                    echo "<li class='calendarmenu'><a href='createapi.php' data-title='CREATE NEW API'><i class='fas fa-calendar-alt menu-icon' title='CREATE NEW API'></i> CREATE NEW API</a></li>";
                    echo "<li class='calendarmenu'><a href='subscription.php' data-title='SUBSCRIPTION'><i class='fas fa-calendar-alt menu-icon' title='SUBSCRIPTION'></i> SUBSCRIPTION</a></li>";
                    echo "<li class='calendarmenu'><a href='Logout.php' data-title='Logout'><i class='fas fa-calendar-alt menu-icon' title='Calendar'></i> Logout</a></li>";
                }
                ?>


            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>

<div class="mobimenu">
    <input type="checkbox" id="overlay-input" />
    <label for="overlay-input" id="overlay-button"><span></span></label>
    <div id="overlay">
        <ul>
            <li class="dashboardmenu"><a href="index.php" data-title='Dashboard'><i class="menu-icon fa fa-laptop tool"></i><br />Dashboard </a></li>

            <?php
            $userrole = $role->user_role;
            if ($userrole == 'superadmin' || $userrole == 'admin') {
                echo "<li class='usersmenu'><a href='users.php' data-title='User'><i class='fas fa-users menu-icon' title='users'></i><br />Users</a></li>";
            }
            ?>
            <li class='calendarmenu'><a href='API_Lists.php' data-title='API Lists'><i class='fas fa-calendar-alt menu-icon' title='API Lists'></i> API LISTS </a></li>
            <li class='calendarmenu'><a href='my_profile.php' data-title='MyProfile'><i class='fas fa-calendar-alt menu-icon' title='MyProfile'></i> MYPROFILE</a></li>
            <li class='calendarmenu'><a href='my_profile.php' data-title='CREATE NEW API'><i class='fas fa-calendar-alt menu-icon' title='CREATE NEW API'></i> CREATE NEW API</a></li>
            <li class='calendarmenu'><a href='my_profile.php' data-title='SUBSCRIPTION'><i class='fas fa-calendar-alt menu-icon' title='SUBSCRIPTION'></i> SUBSCRIPTION</a></li>
            <li class='calendarmenu'><a href='Logout.php' data-title='Logout'><i class='fas fa-calendar-alt menu-icon' title='Calendar'></i> Logout</a></li>
        </ul>
    </div>
</div><!-- mobimenu -->