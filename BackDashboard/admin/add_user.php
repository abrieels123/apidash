<?php include("includes/header.php"); ?>
        
    <?php if(!$session->is_signed_in()) {header("Location: index.php"); } ?>
    
    <?php
$user = new User();
     
   if(isset($_POST['create'])) {
       
       if($user) {

       $user->user_username         = $_POST['username'];
       $user->user_firstname        = $_POST['first_name'];
       $user->user_surname          = $_POST['surname'];
       $password                    = $_POST['password'];
       $hashed_password             = password_hash($password, PASSWORD_DEFAULT);
       $user->user_password         = $hashed_password;
       $user->user_contact_number   = $_POST['contact_number'];
       $user->user_email            = $_POST['email'];
       $user->user_role             = $_POST['role'];
       $user->user_status           = 'active';      
       $user->save();    
            header("Location: Users.php");
       }

}


?>


            <!-- Left Panel -->
       <?php include "includes/navigation.php" ?> 
    
    <!-- /#left-panel -->

    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <?php include "includes/top-header.php" ?>
        <!-- /#header -->
        <!-- Content -->
       
                        <div class="row">
                    <div class="col-lg-12">
                    <h1 class="page-header">ADD USER</h1>
                       
                       <div class="col-md-6 offset-md-3">
                          
                        <form action="" method="post" class="form-group" enctype="multipart/form-data">                        
                         
                           <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" name="username" class="form-control" >
                            </div>    
                     
                              
                           <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input  type="text" name="first_name" class="form-control" >
                            </div> 
                              
                           <div class="form-group">
                                <label for="surname">Surname</label>
                                <input  type="text" name="surname" class="form-control" >
                            </div>     
                              
                           <div class="form-group">
                                <label for="password">Password</label>
                                <input  type="password" name="password" class="form-control" >
                            </div>  
                                       
                           <div class="form-group">
                                <label for="contact_number">Contact Number</label>
                                <input  type="text" name="contact_number" class="form-control" >
                            </div>  
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input  type="email" name="email" class="form-control" >
                            </div>  
                            <div class="form-group">
                                <label for="role">User Role</label>
                                <select name="role" id="" class="form-control">
                                    <option value="superadmin">Superadmin</option>
                                       <option value="superadmin">Admin</option>
                                    <option value="agent">Agent</option>
                                </select>
                            </div> 
                                <div class="form-group">
                                <input  type="submit" name="create" class="btn btn-primary pull-right" >
                            </div>                           
                           </form>
                          
                       </div>
                              
                          
                       
                    </div>
                    
                    
                </div>
     
    </div>
    <!-- /#right-panel -->
       <!-- Footer -->

       <?php include("includes/footer.php"); ?>
        <!-- /.site-footer -->