<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package thursday
 */

?>

<div class="footercta">
    <div class="footerwrap">

    <div class="section group">
	    <div class="col span_1_of_4">
	        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("FooterContact") ) : ?>
			<?php endif;?>
	    </div>
	    <div class="col span_1_of_4">
	        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("FooterQuicklinks") ) : ?>
			<?php endif;?>
	    </div>
	    <div class="col span_1_of_4">
	        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("FooterBrands") ) : ?>
			<?php endif;?>
	    </div>
	    <div class="col span_1_of_4">
	        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("FooterSocial") ) : ?>
			<?php endif;?>
	    </div>
    </div>
    
    <div class="footercopyright">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("FooterCopyright") ) : ?>
			<?php endif;?>
	</div>
	<div class="bordertop"></div>
	<p class="copyrighttxt">Copyright &copy; <?php echo date("Y"); ?> API BOT | <a href="https://rightclickerstesting.co.za/rcm1louise/acbackup/privacy-policy-2/">Privacy Policy</a> | <a href="https://rightclickerstesting.co.za/rcm1louise/acbackup/blog/">Articles</a> | <a href="https://rightclickerstesting.co.za/rcm1louise/acbackup/terms-and-conditions/">Terms and Conditions</a></p>

			<!--<h2 style="text-align: center;"><em><strong><span style="color: #3a67b1;">AC </span><span style="color: #ee2729;">Direct</span></strong></em> Offers Free Delivery Anywhere in SA</h2>-->
			<!--<h2 style="text-align: center;">Best Advice - Best Price - Guaranteed!</h2>-->


			<!--<p style="text-align: center;"><strong><span style="color: #3a67b1;">For Help or Advice?</span></strong> | <a href="tel:0104437840"><strong>JHB:</strong> 010 443 7840</a> | <a href="tel:0212043710"><strong>CPT:</strong> 021 204 3710</a>  | <a href="tel:0319419965"><strong>DBN:</strong> 031 941 9965</a> <br>-->
   <!--   <strong>INT</strong> | <a href="tel:+27 16 365 6685"><i class="fas fa-phone"></i> +27 16 365 6685</a>  |  <a href="tel:0861836782"><i class="fas fa-phone"></i> 086 183 6782</a>-->
    
    </div>
</div>
