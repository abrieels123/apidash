<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package thursday
 */

get_header();
?>

<?php get_template_part( 'template-parts/hero-image', 'page' ); ?>

	<div class="content-area" id="primary">
	<main class="site-main" id="main">
		<div class="content">
		    
		    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("HomeCategories") ) : ?>
			<?php endif;?>
			
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Home Content") ) : ?>
			<?php endif;?>
			
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("LOOKNOFURTHER") ) : ?>
			<?php endif;?>
			<!--<?php // echo do_shortcode('[woo-product-slider-pro id="15910"]'); ?>-->
			
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("ProductCategories") ) : ?>
			<?php endif;?>

			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("PRODUCTSLIDERPRO") ) : ?>
			<?php endif;?>



<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("MOBILECATEGORIES") ) : ?>
<?php endif;?>

<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("MAINCONTENTHOME") ) : ?>
<?php endif;?>


	</main><!-- #main -->
</div><!-- #primary -->


<?php
get_footer();