<?php
/**
Template Name: Attributes
*/

get_header();
get_header( 'shop' );


/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
?>
<div id="journeywrap">
    <div class="journey-thumbnail"></div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		
		<?php
$params = array('posts_per_page' => 5); // (1)
$wc_query = new WP_Query( array(
   'post_type'      => array('product'),
   'post_status'    => 'publish',
   'posts_per_page' => -1,
//   'meta_query'     => array( array(
//        'key' => '_visibility',
//        'value' => array('catalog', 'visible'),
//        'compare' => 'IN',
//    ) ),
//   'tax_query'      => array( array(
//        'taxonomy'        => 'pa_product_details',
//        'field'           => 'slug',
//        'terms'           =>  array($_GET['attributes']),
//        'operator'        => 'AND',
//    ) )
    
    'tax_query' => array(
    'relation' => 'AND',
    array(
        'taxonomy' => 'pa_product_details',
        'field'    => 'slug',
        'terms'    => array($_GET['attributes']),
        'operator'        => 'AND',
    ),
		array(
        'taxonomy' => 'pa_product_details',
        'field'    => 'slug',
        'terms'    => array($_GET['btus']),
        'operator'        => 'AND',
    ),
		array(
        'taxonomy' => 'pa_product_details',
        'field'    => 'slug',
        'terms'    => array($_GET['kws']),
        'operator'        => 'AND',
    ),
    array(
        'taxonomy' => 'pa_product_details',
        'field'    => 'slug',
        'terms'    => $_GET['type'],
		'operator'        => 'AND',
    ),
		    array(
        'taxonomy' => 'pa_product_details',
        'field'    => 'slug',
        'terms'    => $_GET['use'],
		'operator'        => 'AND',
    ),		    
        array(
        'taxonomy' => 'pa_product_details',
        'field'    => 'slug',
        'terms'    => $_GET['location'],
		'operator'        => 'AND',
    ),
        array(
        'taxonomy' => 'pa_brands',
        'field'    => 'slug',
        'terms'    => array($_GET['brand'],$_GET['branda'],$_GET['brandb'],$_GET['brandc'],$_GET['brandd']),
		'operator'        => 'IN',
    ),         
       

),
) ); // (2)
?>

<div class="woocommerce journey">
<h1>PRODUCTS SUGGESTED FOR YOU</h1>
<h4>These are products that fit your needs. Browse different options and add to your cart or view more information on the products</h4>


<ul class="products columns-4">
<?php if ($wc_query->have_posts()) : // (3) ?>
    <?php woocommerce_product_loop_start(); ?>

<?php while ($wc_query->have_posts()) : // (4)
                $wc_query->the_post(); // (4.1) ?>
<?php //the_title(); // (4.2) ?>
<?php wc_get_template_part( 'content', 'product' ); ?>
<?php endwhile; ?>
    <?php woocommerce_product_loop_end(); ?>

<?php else:  ?>
<p>
     <?php _e( '<h3>NO PRODUCTS FOUND</h3><br /><a class="button" href="https://rightclickerstesting.co.za/rcm1louise/acbackup/questionarre/">BACK TO QUESTIONNAIRE</a>' ); // (6) ?>
</p>
<?php endif; ?>
   
    </ul>
    <script type="text/javascript">
var cancel1 = function() {
  history.back();
};
   </script>
 <input type="button" id="refreshed" value="GO BACK TO JOURNEY" onclick="cancel1()" />


</div>
		<?php
		
//// The query
//$products = new WP_Query( array(
//   'post_type'      => array('product'),
//   'post_status'    => 'publish',
//   'posts_per_page' => -1,
////   'meta_query'     => array( array(
////        'key' => '_visibility',
////        'value' => array('catalog', 'visible'),
////        'compare' => 'IN',
////    ) ),
//   'tax_query'      => array( array(
//        'taxonomy'        => 'pa_product_details',
//        'field'           => 'slug',
//        'terms'           =>  array($_GET['attributes']),
//        'operator'        => 'AND',
//    ) )
//) );
//            
////            $products = $_GET['attributes']
//            
//        
//// The Loop
//if ( $products->have_posts() ): while ( $products->have_posts() ):
//    $products->the_post();
//    $product_ids[] = $products->post->ID;
//endwhile;
//    wp_reset_postdata();
//endif;
//            
//            
//
//// TEST: Output the Products IDs
//print_r($product_ids);
            
            
  //   echo do_shortcode( '[products columns="4" attribute="product_details" terms=" ' . $_GET['attributes'] . '" terms_operator="AND"]' ); 

		?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- journeywrap -->

<?php
 get_footer();