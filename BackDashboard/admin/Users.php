<?php include("includes/header.php"); ?>

    <?php if(!$session->is_signed_in()) {header("Location: ../index.php"); } ?>
    
    <?php
    
    $UserRole       = User::find_all_role();
    $Status         = (new User)->change_user_status();
    $User           = User::find_all();
    
?>
            <!-- Left Panel -->
       <?php include "includes/navigation.php" ?> 
    
    <!-- /#left-panel -->

    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <?php include "includes/top-header.php" ?>
        <!-- /#header -->
        <!-- Content -->
        <h1 class="page-header">USERS
              <a href="add_user.php" class="btn btn-dark pull-right">ADD USER</a>         
        </h1>
                        <div class="row">
                    <div class="col-lg-12">
                                              
                       <div class="col-md-12">
                           
                           
                           <table class="table table-hover">
                               
                               <thead>
                                   <tr>
                                       <th>ID</th>
                                       <th>Status</th>
                                       <th>View</th>
                                       <th>Username</th>
                                       <th>First Name</th>
                                       <th>Surname</th>
                                       <th>Number</th>
                                       <th>Email</th>
                                       <th>Province</th>
                                       <th>Area</th>
                                       <th>User Level</th>
                                   </tr>
                                   
                               </thead>
                               <tbody>
                                   <?php foreach ($User as $user) : ?>
                                   
                                   <tr>
                                      <td><?php echo $user->id; ?></td>
                                      <td><?php echo $user->user_status; ?><br />
                                <a href='Users.php?approve=<?php echo $user->id ?>'><i class='fa fa-thumbs-up'></i></a> 
                                <a href='Users.php?unapprove=<?php echo $user->id ?>'><i class='fa fa-thumbs-down'></i></a>
                            </td>
                                       <td><a href="the_installer.php?id=<?php echo $user->id; ?>"><img class="admin-user-thumbnail user_image" src="<?php echo $user->image_path_placeholder(); ?>" alt="" ></a></td>
                                       <td><?php echo $user->user_username; ?>
                                       
                                           <div class="action_links">
                                           <a class="delete_user" href="delete_user.php?id=<?php echo $user->id ?>">Delete</a>
                                           <a class="edit_user" href="edit_user.php?id=<?php echo $user->id ?>">Edit</a>
                                       </div>
                                       
                                       </td>
                                       <td><?php echo $user->user_firstname; ?></td>
                                       <td><?php echo $user->user_surname; ?></td>
                                       <td><a href="tel:<?php echo $user->user_contact_number; ?>"><i class="fas fa-phone"></i></a></td>
                                       <td><a href="mailto:<?php echo $user->user_email; ?>"><i class="fas fa-envelope"></i></a></td>
                                       <td><?php echo $user->user_province; ?></td>
                                       <td><?php echo $user->user_area; ?></td>
                                       <td><?php echo $user->user_role; ?></td>
                                      
                                   </tr>
                                   
                                   <?php endforeach; ?>
                               </tbody>
                               
                           </table> <!--END TABLE -->
                           
                       </div>
                       
                    </div>
                </div>
  
    </div>
    <!-- /#right-panel -->
        <script>
            $(document).ready( function() {
                $('li.usersmenu').addClass( 'active' );
            } );
        </script>
    <div class="clearfix"></div>
        <!-- Footer -->

        <?php include("includes/footer.php"); ?>
        <!-- /.site-footer -->

  
    

 