<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package thursday
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '240529309972943');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=240529309972943&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

	<!-- Google Tag Manager -->
<!-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NCVPCQP');</script> -->
<!-- End Google Tag Manager -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<!--<link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,700" rel="stylesheet">-->
	<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,300;0,400;0,700;0,900;1,900&display=swap" rel="stylesheet">
<script src="https://app.mobicredwidget.co.za/assets/js/instalment.js"></script>
<script src="https://code.jquery.com/jquery-1.8.2.js"></script>
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.11.2/css/all.css" integrity="sha384-zrnmn8R8KkWl12rAZFt4yKjxplaDaT7/EUkKm7AovijfrQItFWR7O/JJn4DAa/gx" crossorigin="anonymous">
<script defer src="https://pro.fontawesome.com/releases/v5.13.1/js/all.js" integrity="sha384-RFburpZVVirucdjarLLIoBiT+LY5YuPB8XboZYWRuWpElr46csH7Ia2nsHfkfBZF" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	



	<?php wp_head(); ?>
	<script>

//jQuery(function(){
////var overlay = jQuery('<div id="overlay"></div>');
////overlay.show();
////overlay.appendTo(document.body);
//jQuery('.popup-onload').show();
//jQuery('.close').click(function(){
//jQuery('.popup-onload').hide();
//overlay.appendTo(document.body).remove();
//return false;
//});
//    
//    jQuery('.x').click(function(){
//jQuery('.popup').hide();
//overlay.appendTo(document.body).remove();
//return false;
//});
//});

</script>
	<script>
window.addEventListener('scroll', function () {
  document.body.classList[
    window.scrollY > 20 ? 'add': 'remove'
  ]('scrolled');
});
</script>

	<!-- Global site tag (gtag.js) - Google Ads: 815181947 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-815181947"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-815181947');
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
<script>
    $(window).scroll(function(){
    if ($(this).scrollTop() > 150) {
       $('header').addClass('smaller');
    } else {
       $('header').removeClass('smaller');
    }
});
</script>
<style>
    .left-radiusTh
        {
            border-top-left-radius: 20px;
        }
        
        .right-radiusTh
        {
            border-top-right-radius: 20px;
        }
        .quoteDescTh
        {
            text-align: center;
            width:50%;
            border-right: 1px solid #ddd;
            padding:10px; 
        }
        .quantityTh
        {
            width:15%;
            text-align: center;
            border-right: 1px solid #ddd;
            padding:10px;
        }
        .totalTh
        {
            width:15%;
            text-align: center;
            padding:10px;
        } 
        .quoteDescTh1
        {
            text-align: center;
            width:50%;
            font-weight: bold;
            border-right: 1px solid #ddd;
            padding:10px;
        }
        .quantityTh1
        {
            width:15%;
            text-align: center;
            font-weight: bold;
            padding:10px;
        }
        .totalTh1
        {
            width:15%;
            text-align: center;
            font-weight: bold;
            padding:10px;
        }
        .sideTotal
        {
            text-align: right;
            border-right: 1px solid #ddd;
            padding:10px;
        }
        .totalThnn
        {
            width:15%;
            text-align: center;
        }
        .borderTd
        {
           border: 1px solid #ddd; 
        }
</style>

</head>

<body <?php body_class(); ?>>
	<?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
	<!-- Google Tag Manager (noscript) -->
<!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NCVPCQP" -->
<!-- height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
<!-- End Google Tag Manager (noscript) -->


<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'thursday' ); ?></a>

<header id="masthead" class="site-header">
	    
	    <div id="ctawidget">
            <div class="ctawidget">
                <?php if ( dynamic_sidebar('widget-area-11') ) : else : endif; ?>
            </div>
        </div>
		
<div class="section group headspace">
    <div class="headspacewrap">
    <div class="colhe span_1_of_4">
				<div class="site-branding">
					<?php
					the_custom_logo();
					if ( is_front_page() && is_home() ) :
						?>
						 <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1> 
						<?php
					else :
						?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
						<?php
					endif;
					$thursday_description = get_bloginfo( 'description', 'display' );
					if ( $thursday_description || is_customize_preview() ) :
						?>
						 <p class="site-description"><?php echo $thursday_description; /* WPCS: xss ok. */ ?></p> 
					<?php endif; ?>
				</div> <!-- .site-branding -->
    </div>
    <div class="colhe span_1_of_3he">
        <!-- search --> <?php if ( dynamic_sidebar('menu-search-bar') ) : else : endif; ?> <!-- search -->
    </div>
    <div class="colhe span_1_of_3he">
        <nav >
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'thursday' ); ?></button>
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
					?>
				</nav>  <!-- #site-navigation -->
    </div>
</div>
</div><!-- headspacewrap -->
		
		

	</header><!-- #masthead -->
	<div id="content" class="site-content">
	    
	