<?php
/**
 * thursday functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package thursday
 */

if ( ! function_exists( 'thursday_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function thursday_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on thursday, use a find and replace
		 * to change 'thursday' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'thursday', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'thursday' ),
			'menu-2' => esc_html__( 'Secondary', 'thursday' )
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'thursday_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'thursday_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function thursday_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'thursday_content_width', 640 );
}
add_action( 'after_setup_theme', 'thursday_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function thursday_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'thursday' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'thursday' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

}

add_action( 'widgets_init', 'thursday_widgets_init' );

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Home Sidebar', 'thursday'),
        'description' => __('Description for this widget-area...', 'thursday'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
	

     // Define Sidebar Widget Area 3
    register_sidebar(array(
        'name' => __('PRICESLIDE', 'thursday'),
        'description' => __('Description for this widget-area...', 'thursday'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('CATEGORIES', 'thursday'),
        'description' => __('Description for this widget-area...', 'thursday'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area Home Content
    register_sidebar(array(
        'name' => __('Home Content', 'thursday'),
        'description' => __('Description for this widget-area...', 'thursday'),
        'id' => 'home-content',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 3
    register_sidebar(array(
        'name' => __('WEATHER', 'thursday'),
        'description' => __('Description for this widget-area...', 'thursday'),
        'id' => 'widget-area-3',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

		// Define Sidebar Widget Area 4
		    register_sidebar(array(
		        'name' => __('MOBILEBAR', 'thursday'),
		        'description' => __('Description for this widget-area...', 'thursday'),
		        'id' => 'widget-area-4',
		        'before_widget' => '<div id="%1$s" class="%2$s">',
		        'after_widget' => '</div>',
		        'before_title' => '<h3>',
		        'after_title' => '</h3>'
		    ));

				// Define Sidebar Widget Area 5
				    register_sidebar(array(
				        'name' => __('MOBILECATEGORIES', 'thursday'),
				        'description' => __('Description for this widget-area...', 'thursday'),
				        'id' => 'widget-area-5',
				        'before_widget' => '<div id="%1$s" class="%2$s">',
				        'after_widget' => '</div>',
				        'before_title' => '<h3>',
				        'after_title' => '</h3>'
				    ));

						// Define Sidebar Widget Area 6
								register_sidebar(array(
										'name' => __('LOOKNOFURTHER', 'thursday'),
										'description' => __('Description for this widget-area...', 'thursday'),
										'id' => 'widget-area-6',
										'before_widget' => '<div id="%1$s" class="%2$s">',
										'after_widget' => '</div>',
										'before_title' => '<h3>',
										'after_title' => '</h3>'
								));

								// Define Sidebar Widget Area 7
										register_sidebar(array(
												'name' => __('PRODUCTSLIDERPRO', 'thursday'),
												'description' => __('Description for this widget-area...', 'thursday'),
												'id' => 'widget-area-7',
												'before_widget' => '<div id="%1$s" class="%2$s">',
												'after_widget' => '</div>',
												'before_title' => '<h3>',
												'after_title' => '</h3>'
										));

										// Define Sidebar Widget Area 8
												register_sidebar(array(
														'name' => __('MAINCONTENTHOME', 'thursday'),
														'description' => __('Description for this widget-area...', 'thursday'),
														'id' => 'widget-area-8',
														'before_widget' => '<div id="%1$s" class="%2$s">',
														'after_widget' => '</div>',
														'before_title' => '<h3>',
														'after_title' => '</h3>'
												));

												// Define Sidebar Widget Area 9
														register_sidebar(array(
																'name' => __('COUNTER', 'thursday'),
																'description' => __('Description for this widget-area...', 'thursday'),
																'id' => 'widget-area-9',
																'before_widget' => '<div id="%1$s" class="%2$s">',
																'after_widget' => '</div>',
																'before_title' => '<h3>',
																'after_title' => '</h3>'
														));			
	
	// Define Sidebar Widget Area 10
												register_sidebar(array(
														'name' => __('SIDEBARSEARCH', 'thursday'),
														'description' => __('Description for this widget-area...', 'thursday'),
														'id' => 'widget-area-10',
														'before_widget' => '<div id="%1$s" class="%2$s">',
														'after_widget' => '</div>',
														'before_title' => '<h3>',
														'after_title' => '</h3>'
												));
												
						// Define Sidebar Widget Area 11
						register_sidebar(array(
							'name' => __('JOURNEYCTA', 'thursday'),
							'description' => __('Description for this widget-area...', 'thursday'),
							'id' => 'widget-area-11',
							'before_widget' => '<div id="%1$s" class="%2$s">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						));		
	
							// Define Sidebar Widget Area 12
						register_sidebar(array(
							'name' => __('catTooltips', 'thursday'),
							'description' => __('Description for this widget-area...', 'thursday'),
							'id' => 'widget-area-12',
							'before_widget' => '<div id="%1$s" class="%2$s">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						));		
    
							// Define Sidebar Widget Area 13
						register_sidebar(array(
							'name' => __('productSortJourney', 'thursday'),
							'description' => __('Description for this widget-area...', 'thursday'),
							'id' => 'widget-area-13',
							'before_widget' => '<div id="%1$s" class="%2$s">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						));	
						
						// Define Sidebar Widget Area 14
						register_sidebar(array(
							'name' => __('HomeCategories', 'thursday'),
							'description' => __('Description for this widget-area...', 'thursday'),
							'id' => 'widget-area-14',
							'before_widget' => '<div id="%1$s" class="%2$s">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						));	
						
						// Define Sidebar Widget Area 15
						register_sidebar(array(
							'name' => __('ProductCategories', 'thursday'),
							'description' => __('Description for this widget-area...', 'thursday'),
							'id' => 'widget-area-15',
							'before_widget' => '<div id="%1$s" class="%2$s">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						));	
						
						// Footer
						// Define Sidebar Widget Area 16
						register_sidebar(array(
							'name' => __('FooterContact', 'thursday'),
							'description' => __('Description for this widget-area...', 'thursday'),
							'id' => 'widget-area-16',
							'before_widget' => '<div id="%1$s" class="%2$s">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						));
						
						// Define Sidebar Widget Area 17
						register_sidebar(array(
							'name' => __('FooterQuicklinks', 'thursday'),
							'description' => __('Description for this widget-area...', 'thursday'),
							'id' => 'widget-area-17',
							'before_widget' => '<div id="%1$s" class="%2$s">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						));
						
						// Define Sidebar Widget Area 18
						register_sidebar(array(
							'name' => __('FooterBrands', 'thursday'),
							'description' => __('Description for this widget-area...', 'thursday'),
							'id' => 'widget-area-18',
							'before_widget' => '<div id="%1$s" class="%2$s">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						));
						
						// Define Sidebar Widget Area 19
						register_sidebar(array(
							'name' => __('FooterSocial', 'thursday'),
							'description' => __('Description for this widget-area...', 'thursday'),
							'id' => 'widget-area-19',
							'before_widget' => '<div id="%1$s" class="%2$s">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						));
						
						// Define Sidebar Widget Area 20
						register_sidebar(array(
							'name' => __('FooterCopyright', 'thursday'),
							'description' => __('Description for this widget-area...', 'thursday'),
							'id' => 'widget-area-20',
							'before_widget' => '<div id="%1$s" class="%2$s">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						));
						
						// Define Sidebar Widget Area 21
						register_sidebar(array(
							'name' => __('Menu Search Bar', 'thursday'),
							'description' => __('Description for this widget-area...', 'thursday'),
							'id' => 'menu-search-bar',
							'before_widget' => '<div id="%1$s" class="%2$s">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						));

}

/**
 * Enqueue scripts and styles.
 */
function thursday_scripts() {
	wp_enqueue_style( 'thursday-style', get_stylesheet_uri() );
    wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'thursday-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'thursday-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'thursday_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

add_action( 'after_setup_theme', 'yourtheme_setup' );

function yourtheme_setup() {
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
}

add_shortcode('wpbsearch', 'get_search_form');

 function wpbsearchform( $form ) {

     $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
     <div><label class="screen-reader-text" for="s">' . __('Search for:') . '</label>
     <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="SEARCH + ENTER"/>
     <input type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
     </div>
     </form>';

     return $form;
 }

 add_shortcode('wpbsearch', 'wpbsearchform');

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

add_theme_support('woocommerce');

/**
 * Add the product's short description (excerpt) to the WooCommerce shop/category pages. The description displays after the product's name, but before the product's price.
 *
 * Ref: https://gist.github.com/om4james/9883140
 *
 * Put this snippet into a child theme's functions.php file
 */
function woocommerce_after_shop_loop_item_title_short_description() {
	global $product;
	if ( ! $product->post->post_excerpt ) return;
	?>
	<div itemprop="description">
		<?php echo apply_filters( 'woocommerce_short_description', $product->post->post_excerpt ) ?>
	</div>
	<?php
}

add_action('woocommerce_after_shop_loop_item', 'woocommerce_after_shop_loop_item_title_short_description', 5);

add_action( 'widgets_init', 'wb_extra_widgets' );
/**
 * Register new Widget area for Product Cat sort dropdown.
 */
function wb_extra_widgets() {
	register_sidebar( array(
		'id'            => 'prod_sort',
		'name'          => __( 'Product Cat Sort', 'themename' ),
		'description'   => __( 'This site below Shop Title', 'themename' ),
		'before_widget'	=> '<div>',
		'after_widget'	=> '</div>',
		'before_title'  => '<h3 class="widgetcattitle">',
		'after_title'   => '</h3>'
	) );
}


add_action( 'init', 'my_add_shortcodes' );

function my_add_shortcodes() {

	add_shortcode( 'my-login-form', 'my_login_form_shortcode' );
}

function my_login_form_shortcode() {

	if ( is_user_logged_in() )
		return '<p>You are already logged in!</p>';

	return wp_login_form( array( 'echo' => false ) );
}

add_filter('widget_text', 'do_shortcode');


// WOOCOMMERCE FUNCTIONS

require_once( 'inc/woocommerce_func.php' );

// SVG UPLOAD SUPPORT
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function fix_svg_thumb_display() {
  echo '
    td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail { 
      width: 100% !important; 
      height: auto !important; 
    }
  ';
}
add_action('admin_head', 'fix_svg_thumb_display');

//   BACK AFTER FORM SUBMISSION

add_filter('wp_headers', 'wpse167128_nocache');
function wpse167128_nocache($headers)
{
    unset($headers['Cache-Control']);
    return $headers;
}

//Woocommerce List
/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

function overrule_webhook_disable_limit( $number ) {
    return 999999999999; //very high number hopefully you'll never reach.
}
add_filter( 'woocommerce_max_webhook_delivery_failures', 'overrule_webhook_disable_limit' );
