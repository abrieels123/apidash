<?php
/**
Template Name: AttributesTEST
*/

get_header();
get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
?>

<div class="journey">
    <div class="journey-thumbnail"></div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<div class="woocommerce journey">
<h1>PRODUCTS SUGGESTED FOR YOU</h1>
<h4>These are products that fit your needs. Browse different options and add to your cart or view more information on the products</h4>
		  
        <?php

  if(!function_exists('wc_get_products')) {
    return;
  }

  $paged                   = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
  $ordering                = WC()->query->get_catalog_ordering_args();
  $ordering['orderby']     = array_shift(explode(' ', $ordering['orderby']));
  $ordering['orderby']     = stristr($ordering['orderby'], 'price') ? 'meta_value_num' : $ordering['orderby'];
  $products_per_page       = apply_filters('loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page());

  $featured_products       = wc_get_products(array(
    'meta_key'             => '_price',
    'status'               => 'publish',
    'limit'                => $products_per_page,
    'page'                 => $paged,
    'paginate'             => true,
    'return'               => 'ids',
    'orderby'              => $ordering['orderby'],
    'order'                => $ordering['order'],
      
    'tax_query' => array(
    'relation' => 'AND',
    array(
        'taxonomy' => 'pa_product_details',
        'field'    => 'slug',
        'terms'    => array($_GET['attributes']),
        'operator'        => 'AND',
    ),
		array(
        'taxonomy' => 'pa_product_details',
        'field'    => 'slug',
        'terms'    => array($_GET['btus']),
        'operator'        => 'AND',
    ),
		array(
        'taxonomy' => 'pa_product_details',
        'field'    => 'slug',
        'terms'    => array($_GET['kws']),
        'operator'        => 'AND',
    ),
    array(
        'taxonomy' => 'pa_product_details',
        'field'    => 'slug',
        'terms'    => $_GET['type'],
		'operator'        => 'AND',
    ),
		    array(
        'taxonomy' => 'pa_product_details',
        'field'    => 'slug',
        'terms'    => $_GET['use'],
		'operator'        => 'AND',
    ),		    
        array(
        'taxonomy' => 'pa_product_details',
        'field'    => 'slug',
        'terms'    => $_GET['location'],
		'operator'        => 'AND',
    ),
        array(
        'taxonomy' => 'pa_brands',
        'field'    => 'slug',
        'terms'    => array($_GET['brand'],$_GET['branda'],$_GET['brandb'],$_GET['brandc'],$_GET['brandd'],$_GET['brande']),
		'operator'        => 'IN',
    ),         
       

),  
  ));

  wc_set_loop_prop('current_page', $paged);
  wc_set_loop_prop('is_paginated', wc_string_to_bool(true));
  wc_set_loop_prop('page_template', get_page_template_slug());
  wc_set_loop_prop('per_page', $products_per_page);
  wc_set_loop_prop('total', $featured_products->total);
  wc_set_loop_prop('total_pages', $featured_products->max_num_pages);

  if($featured_products) {
    do_action('woocommerce_before_shop_loop');
    woocommerce_product_loop_start();
      foreach($featured_products->products as $featured_product) {
        $post_object = get_post($featured_product);
        setup_postdata($GLOBALS['post'] =& $post_object);
        wc_get_template_part('content', 'product');
      }
      wp_reset_postdata();
    woocommerce_product_loop_end();
    do_action('woocommerce_after_shop_loop');
  } else {
    do_action('woocommerce_no_products_found');
      
      

 echo '<h3>NO PRODUCTS FOUND</h3><br />';

  }
     
        ?>


<!-- <input type="button" id="refreshed" value="GO BACK TO JOURNEY" onclick="cancel2()" />-->
<input type="button" value="GO BACK TO JOURNEY" onclick="cancel()" />
    <script type="text/javascript">
//var cancel2 = function() {
//  history.go(-1);
//};
//        var cancel = function() {
////  window.location.href = document.referrer;
//             window.history.go(-1);
//};        
        
        function cancel() {
  window.location.href = document.referrer;
//             window.history.go(-1);
};
   </script>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- journeywrap -->

<?php
 get_footer();