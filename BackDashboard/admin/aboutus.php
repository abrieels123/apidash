<?php include("includes/header.php");
define('WP_USE_THEMES', true);
$InvoiceAmount = 0.00;
$ConfirmedAmount = 0.00;
//$_COOKIE["UpdateValues"] ="";
?>
<?php if (!$session->is_signed_in()) {
header("Location: ../index.php");;
} ?>
<?php
$Total = 0;


try {
    $UserRole   = User::find_all_role();
}

//catch exception
catch (Exception $e) {
    echo 'Message: ' . $e->getMessage();
}
?>
<?php foreach ($UserRole as $role) : ?>
<?php endforeach; ?>
<?php




$userrole = $role->user_role;
if ($userrole == 'superadmin' || $userrole == 'admin') {

    try {
        //$Job        = Invoice::find_all_Approved();
    }

    //catch exception
    catch (Exception $e) {
        echo 'Message: ' . $e->getMessage();
    }
} else {
    try {
       // $Job        = Invoice::find_Approved();
    }

    //catch exception
    catch (Exception $e) {
        echo 'Message: ' . $e->getMessage();
    }
}



if (isset($_POST['Search'])) {
     $Job        =  Invoice::SearchApprovedData( $_POST['agent'],$_POST['FromDate'],$_POST['ToDate']);
}
?>


<!-- Left Panel -->
<?php include "includes/navigation.php" ?>

<!-- /#left-panel -->

<!-- Right Panel -->
<div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php include "includes/top-header.php" ?>
    <!-- /#header -->
    <!-- Content -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <form action="" method="post" class="form-group" enctype="multipart/form-data">

        <body>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Approved Quotes</h1>
                    <?php include("includes/search_form.php"); ?>
                    <div class="col-md-12">
                        From Date :
                        <input type="date" id="FromDate" name="FromDate">
                        To Date :
                        <input type="date" id="ToDate" name="ToDate">
                        Agent :

                                <?php
                                $Agents        = Invoice::find_all_Agents();

                                echo '<select id="agent" name="agent">';
                                if ($Agents != "") {
                                    foreach ($Agents as $agt) :
                                        echo $agt->user_username;
                                        echo '<option value="' . $agt->user_username  . '">' . $agt->user_username  . ' </option>';

                                    endforeach;
                                }
                                ?>
                                </select>
                        <input type="submit" name="Search" id="Search" class="btn btn-dark pull-right" value="Search"> 
                        <div class="table-block">
                            <div id="MainTable" style="visibility: visible">
                                <table class="table table-hover" id="details">

                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Quote Number</th>
                                            <th>NAME</th>
                                            <th>SURNAME</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                                <th>Agent</th>
                                            <th>Status</th>
                                            <th>Sub Total</th>
                                            <th>Total AMOUNT</th>
                                            <th>Date</th>


                                        </tr>

                                    </thead>
                                    <tbody id="mytable">
                                        <?php

                                        foreach ($Job as $job) :

                                        ?>

                                            <tr>
                                                <td><?php echo $job->id; ?></td>
                                                <td><?php echo $job->quoteNumber; ?></td>
                                                <td><?php echo $job->name; ?></td>
                                                <td><?php echo $job->surname; ?></td>
                                                <td><?php echo $job->email; ?></td>
                                                <td><?php echo $job->phone; ?></td>
                                                 <td><?php echo $job->agent; ?></td>
                                                <td><?php echo $job->status; ?></td>
                                                <td>R <?php echo $job->subtotal; ?></td>
                                                <td>R <?php echo $job->total; ?></td>
                                                <td><?php echo $job->date; ?></td>                                              
                                                <td><a href="viewquote.php?quoteid=<?php echo $job->quoteNumber;?>"><i class="fas fa-eye"></i></a></td>
                                                <td><a href="editquote.php?quoteid=<?php echo $job->quoteNumber;?>"><i class="far fa-edit"></i></a></td>
                                                <td><a href="deletequote.php?quoteid=<?php echo $job->quoteNumber;?>"><i class="fas fa-trash"></i></td>
                                               
                                            </tr>

                                        <?php endforeach; ?>
                                    </tbody>

                                </table>
                            </div>
                            <!--END TABLE -->
                            <div class="container" id="StatusGrid" style="visibility: hidden;">
                                Quote Number :<input type="text" name="qNumber" id="qNumber" style="width:250px;height:10px;">
                                Status :
                                <select>
                                    <option value=" "> </option>
                                    <option value="Approve">Approve</option>
                                    <option value="Decline">Decline</option>
                                    <option value="CallmeBack">Call Me Back</option>
                                    <option value="Converted">Converted</option>
                                </select>
                                Assign To :
                                <select>
                                    <option value=" "> </option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                            <div class="form-group" id="here1">

                                <div id="exTab2" class="container" style="visibility: hidden">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#1" data-toggle="tab">Product Information</a>
                                        </li>
                                        <li><a href="#2" data-toggle="tab">Billing Information</a>
                                        </li>
                                        <li><a href="#3" data-toggle="tab">Shipping Information</a>
                                        </li>
                                        <li><a href="#4" data-toggle="tab">Activity Trail</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="MainData">
                                        <?php
                                        $qNumber = $_COOKIE["qNumber"];

                                        $ApprovedQuotes        = Invoice::find_ApprovedID($job->quoteNumber);
                                        if ($qNumber != "") {
                                            $ApprovedDetailsQuotes = Invoice::find_ApprovedDetailsID($qNumber);
                                        }

                                        ?>

                                        <div class="tab-pane active" id="1">
                                            <input type="submit" id="AddProduct" name="AddProduct" class="btn btn-dark pull-right" value="Add Products" onclick="OpenWindow();return false;">
                                            <div id="AddProductsDiv" class="tab-pane active" style="visibility: hidden; width: 1200px; height: 300px; overflow-y: scroll;">
                                            </div>
                                            <div>Product Total</div>
                                            <?php
                                            $qNumber = $_COOKIE["qNumber"];
                                            $Total = 0;
                                            if ($qNumber != "") {
                                                foreach ($ApprovedDetailsQuotes as $app) :
                                                    $Total +=  (float)filter_var($app->price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                                                    echo "<div class='form-group'>" . $app->product . " ";
                                                    echo " " . $app->price . "</div>";

                                                endforeach;
                                                echo "<div class='form-group'>SubTotal: " . (float) $Total . " </div> ";
                                                echo "<div class='form-group'>Payment Method: " . "Cash" . " </div> ";
                                                echo "<div class='form-group'>Total: " . (float) $Total . " </div> ";
                                            }
                                            ?>


                                        </div>
                                        <?php
                                        $qNumber = $_COOKIE["qNumber"];
                                        $BillingDetails = Invoice::find_BillingDetails($qNumber);

                                        if ($BillingDetails == null) {
                                            $BillingDetails = Invoice::find_BillingDetailsEmpty($qNumber);
                                        }
                                        ?>
                                        <div class="tab-pane" id="2">
                                            <?php

                                            $qNumber = $_COOKIE["qNumber"];

                                            foreach ($BillingDetails as $det1) :
                                            ?>
                                                <div class="form-group">
                                                    <label for="username">FirstName</label>
                                                    <input type="text" name="FirstName" class="form-control" value="<?php echo $det1->name ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">LastName</label>
                                                    <input type="text" name="LastName" class="form-control" value="<?php echo $det1->surname ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Company Name</label>
                                                    <input type="text" name="CompanyName" class="form-control" value="<?php echo $det1->Company ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Country</label>
                                                    <input type="text" name="Country" class="form-control" value="<?php echo $det1->Country ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Street Address</label>
                                                    <input type="text" name="StreetAddress" class="form-control" value="<?php echo $det1->StreetAddress ?>">
                                                    <input type="text" name="StreetAddress1" class="form-control" value="<?php echo $det1->StreetAddress1 ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Town/City</label>
                                                    <input type="text" name="TownCity" class="form-control" value="<?php echo $det1->TownCity ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Province</label>
                                                    <input type="text" name="Province" class="form-control" value="<?php echo $det1->Province ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Postal Code</label>
                                                    <input type="text" name="PostalCode" class="form-control" value="<?php echo $det1->PostalCode ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Phone</label>
                                                    <input type="text" name="Phone" class="form-control" value="<?php echo $det1->phone ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Email Address</label>
                                                    <input type="text" name="EmailAddress" class="form-control" value="<?php echo $det1->email ?>">
                                                </div>
                                            <?php endforeach; ?>
                                            <div class="form-group">
                                                <input type="submit" name="updateBilling" class="btn btn-dark pull-right" value="updateBilling">
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="3">
                                            <?php
                                            $qNumber = $_COOKIE["qNumber"];

                                            $ShippingDetails = Invoice::find_ShippingDetails($qNumber);
                                            //var_dump($ApprovedDetailsQuotes);
                                            if ($ShippingDetails == null) {
                                                $ShippingDetails = Invoice::find_ShippingDetailsEmpty($qNumber);
                                            }
                                            ?>
                                            <?php
                                            foreach ($ShippingDetails as $det1) :

                                            ?>
                                                <div class="form-group">
                                                    <label for="username">FirstName</label>
                                                    <input type="text" name="sFirstName" class="form-control" value="<?php echo $det1->name ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">LastName</label>
                                                    <input type="text" name="sLastName" class="form-control" value="<?php echo $det1->surname ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Company Name</label>
                                                    <input type="text" name="sCompanyName" class="form-control" value="<?php echo $det1->Company ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Country</label>
                                                    <input type="text" name="sCountry" class="form-control" value="<?php echo $det1->Country ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Street Address</label>
                                                    <input type="text" name="sStreetAddress" class="form-control" value="<?php echo $det1->StreetAddress ?>">
                                                    <input type="text" name="sStreetAddress1" class="form-control" value="<?php echo $det1->StreetAddress1 ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Town/City</label>
                                                    <input type="text" name="sTownCity" class="form-control" value="<?php echo $det1->TownCity ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Province</label>
                                                    <input type="text" name="sProvince" class="form-control" value="<?php echo $det1->Province ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Postal Code</label>
                                                    <input type="text" name="sPostalCode" class="form-control" value="<?php echo $det1->PostalCode ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Phone</label>
                                                    <input type="text" name="sPhone" class="form-control" value="<?php echo $det1->phone ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Email Address</label>
                                                    <input type="text" name="sEmailAddress" class="form-control" value="<?php echo $det1->email ?>">
                                                </div>
                                            <?php endforeach; ?>
                                            <div class="form-group">
                                                <input type="submit" name="updateShipping" class="btn btn-dark pull-right" value="updateShipping">
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="4">
                                            <div>Date Agent Activity</div>
                                            <?php
                                            $qNumber = $_COOKIE["qNumber"];

                                            $ActivityDetails = Invoice::find_Activity($qNumber);
                                            //var_dump($ApprovedDetailsQuotes);
                                            ?>
                                            <?php
                                            foreach ($ActivityDetails as $det1) :

                                            ?>
                                                <label for="username"><?php echo $det1->date ?></label> <label for="username"><?php echo $det1->Agent ?></label><label for="username"><?php echo $det1->Activity ?></label></br>
                                            <?php endforeach; ?>
                                        </div>

                                    </div>
                                </div>



                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </body>
    </form>

</div>
<!-- /#right-panel -->
<script>
   

    function myFunctionSearch() {
        // Declare variables
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function OpenWindow() {
        document.getElementById("AddProductsDiv").style.visibility = 'visible';
        var data = new FormData();
        data.append('requestId', "");
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'qtest.php', true);
        xhr.onprogress = function() {

        };
        xhr.onload = function() {
            document.getElementById("AddProductsDiv").innerHTML = this.responseText;
        }
        xhr.send(data);
    }

    function myFunction() {
        var x = document.getElementById("here1");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    var table = document.getElementById('details');

    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function() {
            document.getElementById("qNumber").value = this.cells[1].innerHTML;

            document.cookie = "qNumber" + "=" + this.cells[1].innerHTML + "; path=/";
            // document.getElementById("exTab2").style.visibility = 'visible';
            // document.getElementById("1").style.visibility = 'visible';
            // document.getElementById("2").style.visibility = 'visible';
            // document.getElementById("3").style.visibility = 'visible';
            // document.getElementById("4").style.visibility = 'visible';
            // document.getElementById("StatusGrid").style.visibility = 'visible';
            // document.getElementById("MainTable").style.visibility = 'hidden';
            // $(".exTab2").load(location.href + " .exTab2");
            // $(".exTab2").load(location.href + " .exTab2");
            window.location.href =  "editquote.php";
        };
    }

    $(document).ready(function() {
        $('li.confirm_jobsmenu').addClass('active');
    });
</script>
<div class="clearfix"></div>
<!-- Footer -->

<?php include("includes/footer.php"); ?>
<!-- /.site-footer -->