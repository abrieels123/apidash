<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package thursday
 */

?>

<div id="mybutton">
    <button class="feedback"><a href="https://acdirect.co.za/shop/">BUY ONLINE</a></button>
</div><!-- mybutton -->

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>-->


<!--
<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init({
  duration: 1200,
})
</script>
-->

<!--
	<div class="coolman">


			<a href="https://acdirect.co.za/how-to-buy-an-aircon/"><img src="https://acdirect.co.za/wp-content/uploads/2018/11/coolcalc.png"></a></div>
-->
<!--
<div class="footerlinks">

			<div class="section group">
	<div class="col span_1_of_4">
		<h4>CONTACT US</h4>
		
		<a href="tel:0104437840"><strong>JHB:</strong> 010 443 7840</a><br />
		<a href="tel:0212043710"><strong>CPT:</strong> 021 204 3710</a><br />
    <a href="tel:0319419965"><strong>DBN:</strong> 031 941 9965</a><br />
		<strong>Int Tel:</strong><a href="tel:+27 16 365 6685"> +27 16 365 6685</a><br />
    
		<strong>Email:</strong> <a href="mailto:info@acdirect.co.za">info@acdirect.co.za</a><br />
		<STRONG>Website:</STRONG> <a href="mailto:www.acdirect.co.za">www.acdirect.co.za</a><br/>
		64 Kroonarend Rd, Randvaal, 1873<br />
		Johannesburg<br />South Africa<br />

	</div>
	<div class="quicklin">
	<div class="col span_1_of_4">
		<h4>QUICK LINKS</h4>
	<a href="https://acdirect.co.za/about-us/">ABOUT US</a><br />	
	<a href="https://acdirect.co.za/blog/">BLOG</a><br />
	<a href="https://acdirect.co.za/wishlist/">WISHLIST</a><br />
	<a href="https://acdirect.co.za/shop/">BUY ONLINE</a><br />
	<a href="https://acdirect.co.za/how-to-buy-an-aircon/">HOW TO BUY AN AIRCON</a><br />
	<a href="https://acdirect.co.za/faq-2/">FAQ</a><br />
	<a href="https://acdirect.co.za/product-category/samsung/">SAMSUNG</a><br />
	</div>
</div>
<div class="bran">
	<div class="col span_1_of_4">

<h4>BRANDS</h4>
<a href="https://acdirect.co.za/product-category/brand/samsung//" target="_blank">SAMSUNG</a><br />
<a href="https://acdirect.co.za/product-category/brand/daikin/" target="_blank">DAIKIN</a><br />
<a href="https://acdirect.co.za/product-category/brand/midea/" target="_blank">MIDEA</a><br />
<a href="https://acdirect.co.za/product-category/brand/alliance/" target="_blank">ALLIANCE</a><br />
<a href="https://acdirect.co.za/product-category/brand/neocool-brand/" target="_blank">NEOCOOL</a><br />
		<a href="https://acdirect.co.za/product-category/brand/fellowes/" target="_blank">FELLOWES</a><br />
		<a href="https://acdirect.co.za/product-category/brand/lg/" target="_blank">LG</a><br />
<a href="https://acdirect.co.za/shop/" target="_blank">GREE</a><br />
<a href="https://acdirect.co.za/shop/" target="_blank">YORK</a><br />
<a href="https://acdirect.co.za/shop/" target="_blank">CARRIER</a><br />

	</div>
	</div>

	<div class="col span_1_of_4">
		<h4>SOCIAL MEDIA</h4>

		<a href="https://www.facebook.com/acdirect/" target="_blank"><i class="fab fa-facebook social1"></i></a>  <a href="https://www.instagram.com/acdirect.co.za/" target="_blank"><i class="fab fa-instagram social2"></i></a>  <a href="https://www.youtube.com/channel/UCHVHG4ytt46zOGKBDDYFoJw" target="_blank"><i class="fab fa-youtube social3"></i></a>
	<br>
	<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FACDirect-1769119486441166%2F&width=96&layout=button&action=like&size=small&show_faces=false&share=true&height=65&appId=340011219345710" width="96" height="25" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
<br />
		<h4>
			BEST PRICE GUARANTEE
		</h4>
	<a href="https://www.pricecheck.co.za/search?search=acdirect&shop=4791" target="_blank"><img src="https://acdirect.co.za/wp-content/uploads/2018/07/pricecheck.png" width="48%"></a>
	</div>
</div>
		</div>
-->
	<footer id="colophon" class="site-footer">
<div class="site-info">
<!--
		
			<div style="text-align:center; margin-bottom:10px;">
Copyright &copy; <?php echo date("Y") ?> API BOT | <a href="https://acdirect.co.za/privacy-policy-2/">Privacy Policy</a> | <a href="https://acdirect.co.za/blog/">Articles</a>
<br /></div>
<div style="text-align:center; margin-bottom:10px;"><a class="button2" href="https://acdirect.co.za/sign-up/" target="_blank"><span class="button_icon"><i class="fas fa-envelope"></i></span><span class="button_label"> Sign Up for the Latest Deals</span></a><br />
<a href="https://live.mobicred.co.za/cgi-bin/wspd_cgi.sh/WService=wsb_mcrliv/run.w?run=application&merchantID=2660&returnUrl=https://acdirect.co.za/" target="_blank"><img src="https://acdirect.co.za/wp-content/uploads/2018/07/payment-banner-3.png"></a>
</div>
		
		 
-->
<!--
		<div id="mybutton">
		  <button class="feedback"><a href="https://acdirect.co.za/shop/">BUY ONLINE</a></button>
		</div>
-->
</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
 <?php get_template_part( 'template-parts/above-footer', 'page' ); ?>
<?php wp_footer(); ?>

</body>
</html>
