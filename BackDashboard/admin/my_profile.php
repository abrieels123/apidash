<?php include("includes/header.php"); ?>
        
    <?php if(!$session->is_signed_in()) { 
      header("Location: index.php");} ?>
    
    
    <?php
    
    if(empty($_SESSION['id'])) {
        
      
        header("Location: users.php");
        
    } 

$current_user = User::find_by_id($_SESSION['id']);
    
    if(isset($_POST['update'])) {
        
        if($current_user) {
            
        $current_user->user_username    = $_POST['username'];
        $current_user->user_firstname   = $_POST['first_name'];
        $current_user->user_surname     = $_POST['surname'];
        $password                       = $_POST['password'];
        $hashed_password                = password_hash($password, PASSWORD_DEFAULT);
        $current_user->user_password    = $hashed_password;
            
            if(empty($_FILES['user_image'])) {
                
                $user->save();
                
            } else {
                
        $current_user->set_file($_FILES['user_image']);
        $current_user->upload_photo(); 
        $current_user->save();
        header("Location: my_profile.php");
                
            }
            

           
        } 
}

?>


<?php include "includes/navigation.php" ?> 
            
           
  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
        <!-- Header-->
        <?php include "includes/top-header.php" ?>
        <!-- /#header -->
        <!-- Content -->
        
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           UPDATE YOUR PROFILE
                        </h1>
                       
                      
                          <div class="col-md-6  offset-md-3">
                              
                              <img class="img-responsive" src="<?php echo $current_user->image_path_placeholder(); ?>" alt="">
                              
                          </div>
                        <form action="" method="post" class="form-group" enctype="multipart/form-data">
                         <div class="col-md-6  offset-md-3">
                         <div class="form-group">
                          <input type="file" name="user_image">
                            </div>
                           <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" name="username" class="form-control" value="<?php echo $current_user->user_username ?>" >
                            </div>    
                     
                              
                           <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input  type="text" name="first_name" class="form-control" value="<?php echo $current_user->user_firstname ?>">
                            </div> 
                              
                           <div class="form-group">
                                <label for="surname">Surname</label>
                                <input  type="text" name="surname" class="form-control" value="<?php echo $current_user->user_surname ?>">
                            </div>     
                              
                           <div class="form-group">
                                <label for="password">Password</label>
                                <input  type="password" name="password" class="form-control" value="<?php echo $current_user->user_password ?>">
                            </div>   
                               
                           <div class="form-group">
                                <input  type="submit" name="update" class="btn btn-dark pull-right" value="UPDATE" >
                            </div>
                            
                                   
                        </div>
                            
                                                                  
                           </form>
                          

                              
                          
                       
                    </div>
                    
                    
                </div>
                <!-- /.row -->

        

  <?php include("includes/footer.php"); ?>