<?php

if (isset($_POST['assign'])) {
        $TheJob         =   Jobs::find_job_id($_GET['id']);
        $installer      = $_POST['installer'];
        $newInstaller   = User::find_jobs_by_user_id($installer);


        foreach($newInstaller as $newins) {
        $newEmail       = $newins->user_email;
        }

        $user_mail            = $newEmail;
        $subject              = "New Installation";
        $style                = 'height:36px;v-text-anchor:middle;width:700px;';
        $style_small          = 'height:36px;v-text-anchor:middle;width:700px;';
        $stylea               = 'color:#ffffff;font-family:Helvetica, Arial,sans-serif;font-size:16px;';
        $style_norm           = 'background-color:#EB7035;border:1px solid #EB7035;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:700px;-webkit-text-size-adjust:none;mso-hide:all;';
        $style_accept           = 'background-color:#37ae50;border:1px solid #37ae50;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:340px;-webkit-text-size-adjust:none;mso-hide:all;';
        $style_reject           = 'background-color:#f72e27;border:1px solid #f72e27;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:340px;-webkit-text-size-adjust:none;mso-hide:all;';
        $outlook_style           = "arcsize='5%' strokecolor='#550062' fillcolor='#550062'"    ;
        $outlook_style_accept    = "arcsize='5%' strokecolor='#37ae50' fillcolor='#37ae50'"    ;
        $outlook_style_reject    = "arcsize='5%' strokecolor='#f72e27' fillcolor='#f72e27'"    ;
    
        $message                   =  "<table cellpadding='5' cellspacing='5' border='0'>";
        $message                  .=  "<tr>";
        $message                  .=  "<th colspan='2'><div style='text-align:center;background-color: #f3f3f3;max-width:700px;color:#550062'><img src='https://rightclickerstesting.co.za/rcm1louise/installers/admin/images/logo.png'><br /><h1 >YOU HAVE A NEW INSTALLATION</h1><p>INSTALLATION DATE:" . $TheJob->install_date . "</p><p>Please visit the installer zone to schedule a time</p><p>Declining the installation will be available for the next 48hours</p><p>&nbsp;</p></div></th>";
        $message                  .=  "</tr>";
        $message                  .=  "<tr style='text-align:center'>";
        $message                  .=  "<td><h3>ORDER NR:</h3></td>";
        $message                  .=  "<td><h3>$TheJob->order_nr</h3></td>";
        $message                  .=  "</tr>";
        $message                  .=  "<tr>";
        $message                  .=  "<td colspan='2'><div style='text-align:center'><a href='https://acdirect.co.za/contact-us/'><img src='https://rightclickerstesting.co.za/rcm1louise/installers/admin/images/exclamation_green.png'></a></div></td>";
        $message                  .=  "</tr>";        
        $message                  .=  "<tr>";
        $message                  .=  "<td colspan='2'>
        <!--[if mso]>
          <v:roundrect xmlns:v='urn:schemas-microsoft-com:vml' xmlns:w='urn:schemas-microsoft-com:office:word' href='https://rightclickerstesting.co.za/rcm1louise/installers' style='". $style . " " .$outlook_style ."' >
            <w:anchorlock/>
            <center style='" . $stylea . "'>VISIT INSTALLER ZONE</center>
          </v:roundrect>
        <![endif]-->

        <a href='https://rightclickerstesting.co.za/rcm1louise/installers/index.php' style='" . $style_norm . "'>VISIT INSTALLER ZONE</a>

        </td>";
        $message                  .=  "</tr>";
        $message                  .=  "<tr>";
        $message                  .=  "<td colspan='2'><div><a href='https://acdirect.co.za/contact-us/'><img src='https://rightclickerstesting.co.za/rcm1louise/installers/admin/images/footer.png'></a></div></td>";
        $message                  .=  "</tr>";
        $message                  .=  "</table>";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $messages = "<html>
        <head>
  	     <title>NEW INSTALLATION</title>
        </head>
        <body>
  	     <p>".$message."</p>
    </body>
  </html>";
    
  if (mail($user_mail, $subject, $messages, $headers)) {
   echo "Email sent";
  }else{
   echo "Failed to send email. Please try again later";
  }
    
        
        $oldId                = $_GET['oldid'];
    
          $oldInstaller   = User::find_jobs_by_user_id($oldId);


        foreach($oldInstaller as $oldins) {
        $oldEmail       = $oldins->user_email;
        }
        $theEmail             = $oldEmail; 
        $installer            = $TheJob->installer;
        $subject              = "Installation Re-assigned";
        $style                = 'height:36px;v-text-anchor:middle;width:700px;';
        $style_small          = 'height:36px;v-text-anchor:middle;width:700px;';
        $stylea               = 'color:#ffffff;font-family:Helvetica, Arial,sans-serif;font-size:16px;';
        $style_norm           = 'background-color:#EB7035;border:1px solid #EB7035;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:315px;-webkit-text-size-adjust:none;mso-hide:all;';
        $style_accept          = 'background-color:#37ae50;border:1px solid #37ae50;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:340px;-webkit-text-size-adjust:none;mso-hide:all;';
        $style_reject          = 'background-color:#f72e27;border:1px solid #f72e27;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:340px;-webkit-text-size-adjust:none;mso-hide:all;';
        $outlook_style          = "arcsize='5%' strokecolor='#550062' fillcolor='#550062'"    ;
        $outlook_style_accept   = "arcsize='5%' strokecolor='#37ae50' fillcolor='#37ae50'"    ;
        $outlook_style_reject   = "arcsize='5%' strokecolor='#f72e27' fillcolor='#f72e27'"    ;
    
        $message                  =  "<table cellpadding='5' cellspacing='5' border='0'>";
        $message                  .=  "<tr>";
        $message                  .=  "<th colspan='2'><div style='text-align:center;background-color: #f3f3f3;max-width:700px;color:#550062'><img src='https://rightclickerstesting.co.za/rcm1louise/installers/admin/images/logo.png'><br /><h1 >ONE OF YOUR INSTALLTIONS HAS BEEN RE-ASSIGNED</h1><p>Your installtion order nr: " . $TheJob->order_nr . " has been re-assigned to another installer and will no longer be in your schedule</p></div></th>";
        $message                  .=  "</tr>";
        $message                  .=  "<tr>";
        $message                  .=  "<td colspan='2'><div style='text-align:center'><a href='https://acdirect.co.za/contact-us/'><img src='https://rightclickerstesting.co.za/rcm1louise/installers/admin/images/exclamation_green.png'></a></div></td>";
        $message                  .=  "</tr>";        
        $message                  .=  "<tr>";
        $message                  .=  "<td colspan='2'  >
        <!--[if mso]>
          <v:roundrect xmlns:v='urn:schemas-microsoft-com:vml' xmlns:w='urn:schemas-microsoft-com:office:word' href='https://rightclickerstesting.co.za/rcm1louise/installers' style='". $style . " " .$outlook_style ."' >
            <w:anchorlock/>
            <center style='" . $stylea . "'>VISIT INSTALLER ZONE</center>
          </v:roundrect>
        <![endif]-->

        <a href='https://rightclickerstesting.co.za/rcm1louise/installers/index.php' style='" . $style_norm . "'>VISIT INSTALLER ZONE</a>

        </td>";
        $message                  .=  "</tr>";
        $message                  .=  "<tr>";
        $message                  .=  "<td colspan='2'><div><a href='https://acdirect.co.za/contact-us/'><img src='https://rightclickerstesting.co.za/rcm1louise/installers/admin/images/footer.png'></a></div></td>";
        $message                  .=  "</tr>";
        $message                  .=  "</table>";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $messages = "<html>
        <head>
  	     <title>NEW INSTALLATION</title>
        </head>
        <body>
  	     <p>".$message."</p>
    </body>
  </html>";
    
  if (mail($theEmail, $subject, $messages, $headers)) {
   echo "Email sent";
  }else{
   echo "Failed to send email. Please try again later";
  }
} 


?>