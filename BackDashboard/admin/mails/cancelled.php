<?php
             if($StatusCancelled) {
              echo "yes";
               // START MAIL THE CLIENT
              
  $Accepted = Jobs::find_job_id($_GET['cancel']);
            
  $client_mail              = $Accepted->email;                         
  $date                     = $Accepted->install_date;
  $subject                  = "Aircon Installation Cancelled";
  $style                    = 'height:36px;v-text-anchor:middle;width:700px;';
  $style_small              = 'height:36px;v-text-anchor:middle;width:340px;';
  $stylea                   = 'color:#ffffff;font-family:Helvetica, Arial,sans-serif;font-size:16px;';
  $style_norm               = 'background-color:#EB7035;border:1px solid #EB7035;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:700px;-webkit-text-size-adjust:none;mso-hide:all;';
  $style_accept             = 'background-color:#37ae50;border:1px solid #37ae50;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:340px;-webkit-text-size-adjust:none;mso-hide:all;';
  $style_reject             = 'background-color:#f72e27;border:1px solid #f72e27;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:340px;-webkit-text-size-adjust:none;mso-hide:all;';
  $outlook_style            = "arcsize='5%' strokecolor='#550062' fillcolor='#550062'"    ;
  $outlook_style_accept     = "arcsize='5%' strokecolor='#37ae50' fillcolor='#37ae50'"    ;
  $outlook_style_reject     = "arcsize='5%' strokecolor='#f72e27' fillcolor='#f72e27'"    ;
    

    $msg                   =  "<table cellpadding='5' cellspacing='5' border='0' style='max-width:700px'>";
    $msg                  .=  "<tr>";
    $msg                  .=  "<th colspan='2'><div style='text-align:center;background-color: #f3f3f3;max-width:700px;color:#550062'><img src='https://rightclickerstesting.co.za/rcm1louise/installers/admin/images/logo.png'><br /><h1 >AIRCON INSTALLATION RE-ACTIVATED</h1></div></th>";
    $msg                  .=  "</tr>";
    $msg                  .=  "<tr>";
    $msg                  .=  "<td colspan='2'>The installation of your aircon scheduled for: $date has been cancelled. Please make sure to fill in the installation checklist to re-activate your installtion. You will be notified when the installtion has been re-activated</td>";
    $msg                  .=  "</tr>";
              
    $msg                  .=  "<tr>";
    $msg                  .=  "<td colspan='2'  >
    <!--[if mso]>
    <v:roundrect xmlns:v='urn:schemas-microsoft-com:vml' xmlns:w='urn:schemas-microsoft-com:office:word' href='https://rightclickerstesting.co.za/rcm1louise/installers/booking_form.php?order=" . $Accepted->order_nr . "' style='". $style . " " .$outlook_style ."' >
    <w:anchorlock/>
    <center style='" . $stylea . "'>COMPLETE CHECKLIST</center>
    </v:roundrect>
    <![endif]-->

    <a href='https://rightclickerstesting.co.za/rcm1louise/installers/booking_form.php?order=" . $Accepted->order_nr . "' style='" . $style_norm . "'>COMPLETE CHECKLIST</a>

    </td>";
    $msg                  .=  "</tr>";     
    $msg                  .=  "<tr>";
    $msg                  .=  "<td colspan='2'><div><a href='https://acdirect.co.za/contact-us/'><img src='https://rightclickerstesting.co.za/rcm1louise/installers/admin/images/footer.png'></a></div></td>";
    $msg                  .=  "</tr>";
    $msg                  .=  "</table>";

  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
  $messages = "<html>
  <head>
  	<title>INSTALLATION CANCELLED</title>
  </head>
  <body>
  	
  	<p>".$msg."</p>
  </body>
  </html>";
  if (mail($client_mail, $subject, $messages, $headers)) {
   echo "Email sent";
  } else {
   echo "Failed to send email. Please try again later";
  } // END MAIL THE CLIENT
              
     // INSTALLER EMAIL 
              
  $installer_mail         = User::find_installers_email($Accepted->installer); 
    foreach($installer_mail as $install_mail){

        $installer_mail            = $install_mail->user_email;

    }
              
  $date                     = $Accepted->install_date;
  $orderNr                  = $Accepted->order_nr;
  $subject                  = "Aircon Installation Cancelled";
  $style                    = 'height:36px;v-text-anchor:middle;width:700px;';
  $style_small              = 'height:36px;v-text-anchor:middle;width:340px;';
  $stylea                   = 'color:#ffffff;font-family:Helvetica, Arial,sans-serif;font-size:16px;';
  $style_norm               = 'background-color:#EB7035;border:1px solid #EB7035;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:700px;-webkit-text-size-adjust:none;mso-hide:all;';
  $style_accept             = 'background-color:#37ae50;border:1px solid #37ae50;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:340px;-webkit-text-size-adjust:none;mso-hide:all;';
  $style_reject             = 'background-color:#f72e27;border:1px solid #f72e27;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:340px;-webkit-text-size-adjust:none;mso-hide:all;';
  $outlook_style            = "arcsize='5%' strokecolor='#550062' fillcolor='#550062'"    ;
  $outlook_style_accept     = "arcsize='5%' strokecolor='#37ae50' fillcolor='#37ae50'"    ;
  $outlook_style_reject     = "arcsize='5%' strokecolor='#f72e27' fillcolor='#f72e27'"    ;
    

    $msg                   =  "<table cellpadding='5' cellspacing='5' border='0' style='max-width:700px'>";
    $msg                  .=  "<tr>";
    $msg                  .=  "<th colspan='2'><div style='text-align:center;background-color: #f3f3f3;max-width:700px;color:#550062'><img src='https://rightclickerstesting.co.za/rcm1louise/installers/admin/images/logo.png'><br /><h1 >AIRCON INSTALLATION CANCELLED</h1></div></th>";
    $msg                  .=  "</tr>";
    $msg                  .=  "<tr>";
    $msg                  .=  "<td colspan='2'>Your aircon installation order number: $orderNr scheduled for: $date has been cancelled. You will be notified when the installtion is re-activated </td>";
    $msg                  .=  "</tr>";

    $msg                  .=  "<tr>";
    $msg                  .=  "<td colspan='2'><div><a href='https://acdirect.co.za/contact-us/'><img src='https://rightclickerstesting.co.za/rcm1louise/installers/admin/images/footer.png'></a></div></td>";
    $msg                  .=  "</tr>";
    $msg                  .=  "</table>";

  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
  $messages = "<html>
  <head>
  	<title>INSTALLATION CANCELLED</title>
  </head>
  <body>
  	
  	<p>".$msg."</p>
  </body>
  </html>";
  if (mail($installer_mail, $subject, $messages, $headers)) {
   echo "Email sent";
  } else {
   echo "Failed to send email. Please try again later";
  } // END MAIL THE INSTALLER
            
          } 

?>